﻿using LendFoundry.Security.Tokens;
using System;


namespace LendFoundry.DrawDownProcessor
{
   public interface IDrawDownProcessorClientFactory
    {

        IDrawDownProcessorService Create(ITokenReader reader);
    }
}
