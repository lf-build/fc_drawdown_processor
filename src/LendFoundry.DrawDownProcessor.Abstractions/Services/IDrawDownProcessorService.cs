﻿using CreditExchange.StatusManagement;
using LendFoundry.DrawDown;
using LendFoundry.ProductRule;
using System.Threading.Tasks;
using LendFoundry.Cashflow;

namespace LendFoundry.DrawDownProcessor
{
    public interface IDrawDownProcessorService
    {
        Task<IDrawDownResponse> AddDrawDown(IDrawDownAddRequest drawDownRequest);
        Task<IProductRuleResult> ComputeOffer(string entityId);
     
     //   Task ACHStatusHandler(string entityType, string entityId, object data);
        Task ACHFailStatusHandler(string entityType, string entityId, object data);

        Task ApproveDrawDown(string drawDownId, string statusWorkFlowId,IRequestModel reasons);

        Task<IDrawDown> UpdateDrawDown(string drawDownId, IDrawDownAddRequest drawDownRequest);

        Task GenerateLocAgreement(string drawDownId );
        Task SendToBorrower(string drawDownId, string docusignReturnUrl);

        Task<DocusignReturnObject> GetAgreementDocument(string drawDownId, string docusignReturnUrl);
        Task<bool> SignDocumentReceivedHandler(string entityType, string entityId, object data);

        Task AddFundingRequest(string drawDownId, IBankAccount bankAccountDetail, bool isResubmit = false);

        Task UpdateFundingStatus(string entityId, string Status);
    }
}
