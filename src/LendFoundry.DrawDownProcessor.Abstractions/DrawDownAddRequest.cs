﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.DrawDownProcessor
{
    public class DrawDownAddRequest : IDrawDownAddRequest
    {
        public string LocNumber { get; set; }
        public double RequestedAmount { get; set; }
        public DateTime NeedByDate { get; set; }
        public DateTime DrawDownDate { get; set; }
    }
}
