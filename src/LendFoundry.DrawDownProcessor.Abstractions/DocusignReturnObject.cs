﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.DrawDownProcessor
{
    public class DocusignReturnObject
    {
        public string url { get; set; }
        public bool isDocusignCreated { get; set; }

    }
}
