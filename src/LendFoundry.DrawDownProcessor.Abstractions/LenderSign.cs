﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.DrawDownProcessor
{
    public class LenderSign
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
