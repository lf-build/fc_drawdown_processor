﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.DrawDownProcessor
{
    public interface ISendAgreementRequest
    {
         string returnUrl { get; set; }
    }
}
