﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.DrawDownProcessor
{
    public class Formatter
    {
        public static string MoneyFormatter(dynamic amount)
        {
            amount = Convert.ToDouble(amount);
            //if (amount < 0.0)
            //    throw new InvalidOperationException("Money value can not be Negative");
           /* CultureInfo cultureInfo = new CultureInfo("en-US");
            cultureInfo.NumberFormat.CurrencyDecimalDigits = 2;
            var formattedAmount = string.Format(cultureInfo, "{0:C}", Convert.ToDecimal(amount), 0);
            return formattedAmount;*/
              string result = string.Empty;
            var usCulture = CultureInfo.CreateSpecificCulture("en-US");
            var clonedNumbers = (NumberFormatInfo)usCulture.NumberFormat.Clone();
            clonedNumbers.CurrencyNegativePattern = 0;
            result = amount.ToString("C", clonedNumbers);
            return result;
        }

    }
}
