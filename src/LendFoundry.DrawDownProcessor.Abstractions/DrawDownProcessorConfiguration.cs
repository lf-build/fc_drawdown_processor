﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.ProductRule;
using LMS.Foundation.Amortization;
using Newtonsoft.Json;

namespace LendFoundry.DrawDownProcessor {
    public class DrawDownProcessorConfiguration {
        public CaseInsensitiveDictionary<string> Statuses { get; set; }
        public List<EventConfiguration> events { get; set; }
        public bool AutoLoanSchedule { get; set; }

        public string FundingSourceId { get; set; }

        public LenderSign LenderSignature { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IPaybackTier, PaybackTier>))]
        public List<IPaybackTier> PaybackTiers { get; set; }
        public string Logo { get; set; }

        public string ProfessionalServiceFeePercentage { get; set; }

        public int NumberOfBusinessDaysInMonth { get; set; }

        public int NumberOfBusinessDaysInWeek { get; set; }
        public int RoundingDigit {get;set;}

        public string  RoundingMethod {get;set;}
        public   List<SlabDetails> SlabDetails = null;

        public List<SlabDetails> SlabDetails8Month = null;

        public string DocumentName {get;set;}
    }
}