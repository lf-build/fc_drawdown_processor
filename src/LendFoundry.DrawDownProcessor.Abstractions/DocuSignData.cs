﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.DrawDownProcessor
{
    public class DocuSignData
    {
        public List<byte[]> DocumentContent { get; set; }
        public List<string> DocumentName { get; set; }
    }
}
