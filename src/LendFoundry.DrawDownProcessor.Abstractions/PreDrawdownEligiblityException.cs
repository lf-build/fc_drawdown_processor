﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.DrawDownProcessor
{
    public class PreDrawdownEligiblityException : Exception
    {
        public PreDrawdownEligiblityException(string message) : base(message)
        {
        }
    }
}
