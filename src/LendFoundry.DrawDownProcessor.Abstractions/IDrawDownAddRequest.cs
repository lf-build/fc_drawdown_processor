﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.DrawDownProcessor
{
    public interface IDrawDownAddRequest
    {
        string LocNumber { get; set; }
        double RequestedAmount { get; set; }
        DateTime NeedByDate { get; set; }
        DateTime DrawDownDate { get; set; }
    }
}
