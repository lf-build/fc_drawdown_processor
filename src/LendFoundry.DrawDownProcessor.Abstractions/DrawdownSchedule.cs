﻿namespace LendFoundry.DrawDownProcessor
{
    public class DrawdownSchedule
    {
        public int MonthId { get; set; }
        public string Principle { get; set; }
        public string Interest { get; set; }
        public string Payment { get; set; }
        public string TotalPayback { get; set; }

    }
}
