﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.DrawDownProcessor
{
    public class OwnerDetails
    {
        public int id { get; set; }
        public string OwnerName { get; set; }
        public string OwnerAdd { get; set; }
    }
}
