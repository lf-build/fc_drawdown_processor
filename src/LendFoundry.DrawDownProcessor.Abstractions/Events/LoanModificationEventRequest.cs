﻿using LMS.LoanManagement.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.DrawDownProcessor.Events
{
    public class LoanModificationEventRequest
    {
        public List<ILoanScheduleDetails> loanSchedule { get; set; }
        public double loanAmount { get; set; }
        public string frequency { get; set; }
        public int term { get; set; }

    }
}
