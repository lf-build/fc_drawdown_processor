﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.DrawDownProcessor.Events
{
    public class PersonalCreditReportAdded : SyndicationCalledEvent
    {
    }
}
