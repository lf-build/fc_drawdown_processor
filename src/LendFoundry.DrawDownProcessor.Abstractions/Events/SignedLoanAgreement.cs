﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.DrawDownProcessor.Events
{
    public class SignedLoanAgreement : SyndicationCalledEvent
    {
    }
}
