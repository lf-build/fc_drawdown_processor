﻿



namespace LendFoundry.DrawDownProcessor
{
    public class DrawDownProcessing
    {               
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string Name { get; set; }
        public string ReferenceNumber { get; set; }
        public object Request { get; set; }
        public object Response { get; set; }
    }
}
