﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.DrawDownProcessor
{
    public class Settings
    {
        private const string Prefix = "DRAW-DOWN-PROCESSOR";

        public static string ServiceName => Prefix.ToLower();

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
        
        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUP", "lookup");
        public static ServiceSettings NumberGenerator { get; } = new ServiceSettings($"{Prefix}_NUMBERGENERATOR", "number-generator");
        public static ServiceSettings StatusManagement { get; } = new ServiceSettings($"{Prefix}_STATUS_MANAGEMENT", "status-management");
        public static ServiceSettings DrawDown { get; } = new ServiceSettings($"{Prefix}_DRAWDOWN_HOST", "draw-down", $"{Prefix}_DRAWDOWN_PORT");

        public static string Nats { get; } = Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
        public static ServiceSettings Product { get; } = new ServiceSettings($"{Prefix}_PRODUCT_HOST", "product", $"{Prefix}_PRODUCT_PORT");
        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATA_ATTRIBUTES", "data-attributes");
        public static ServiceSettings Experian { get; } = new ServiceSettings($"{Prefix}_EXPERIAN_HOST", "experian", $"{Prefix}_EXPERIAN_PORT");
        public static ServiceSettings ProductRule { get; } = new ServiceSettings($"{Prefix}_PRODUCTRULE_HOST", "ProductRule", $"{Prefix}_PRODUCTRULE_PORT");
        public static ServiceSettings Consent { get; } = new ServiceSettings($"{Prefix}_CONSENT_HOST", "consent", $"{Prefix}_CONSENT_PORT");
        public static ServiceSettings businessFunding { get; } = new ServiceSettings($"{Prefix}_BUSINESS-FUNDING", "business-Funding");
        public static ServiceSettings Application { get; } = new ServiceSettings($"{Prefix}_APPLICATION", "application");
        public static ServiceSettings LoanManagement { get; } = new ServiceSettings($"{Prefix}_LOAN-MANAGEMENT", "loan-management");
        public static ServiceSettings Ach { get; } = new ServiceSettings($"{Prefix}_ACH_HOST", "ach", $"{Prefix}_ACH_PORT");
        public static ServiceSettings Cashflow { get; } = new ServiceSettings($"{Prefix}_CASHFLOW_HOST", "cashflow", $"{Prefix}_CASHFLOW_PORT");
       
        public static ServiceSettings Calendar { get; } = new ServiceSettings($"{Prefix}_CALENDAR", "calendar");

        public static ServiceSettings OfferEngine { get; } = new ServiceSettings($"{Prefix}_OFFER-ENGINE", "offer-engine");

        public static ServiceSettings TokenGenerator { get; } = new ServiceSettings($"{Prefix}_TOKEN-GENERATOR_HOST", "token-generator", $"{Prefix}_TOKEN-GENERATOR_PORT");
        public static ServiceSettings EquifaxCreditReport { get; } = new ServiceSettings($"{Prefix}_EQUIFAX_HOST", "equifax", $"{Prefix}_EQUIFAX_PORT");
        public static ServiceSettings DataMerchSyndication { get; } = new ServiceSettings($"{Prefix}_DATAMERCH_HOST", "syndication-datamerch", $"{Prefix}_DATAMERCH_PORT");
        public static ServiceSettings Applicant { get; } = new ServiceSettings($"{Prefix}_APPLICANT", "applicant");

        public static ServiceSettings LoanAccounting { get; } = new ServiceSettings($"{Prefix}_LOAN-ACCOUNTING_HOST", "loan-accounting", $"{Prefix}_LOAN-ACCOUNTING_PORT");
        public static ServiceSettings DocumentGenerator { get; } = new ServiceSettings($"{Prefix}_DOCUMENTGENERATOR_HOST", "document-generator", $"{Prefix}_DOCUMENTGENERATOR_PORT");
        public static ServiceSettings ApplicationDocument { get; } = new ServiceSettings($"{Prefix}_APPLICATIONDOCUMENT_HOST", "application-document", $"{Prefix}_APPLICATIONDOCUMENT_PORT");
        public static ServiceSettings VerificationEngine { get; } = new ServiceSettings($"{Prefix}_VERIFICATIONENGINE_HOST", "verification-engine", $"{Prefix}_VERIFICATIONENGINE_PORT");
        public static ServiceSettings DocuSign { get; } = new ServiceSettings($"{Prefix}_DOCUSIGN_HOST", "docusign", $"{Prefix}_DOCUSIGN_PORT");
    }
}