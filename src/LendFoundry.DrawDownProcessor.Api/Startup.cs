﻿#if DOTNET2
using System;
using System.IO;
using System.Threading.Tasks;
using CreditExchange.Datamerch.Client;
using CreditExchange.StatusManagement.Client;
using LendFoundry.Application.Document.Client;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Business.Funding.Client;
using LendFoundry.Business.OfferEngine.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Cashflow.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocuSign.Client;
using LendFoundry.DrawDown.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.ProductConfiguration.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Equifax.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.Token.Generator.Client;
using LendFoundry.VerificationEngine.Client;
using LMS.LoanAccounting.Client;
using LMS.LoanManagement.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using LMS.Foundation.Amortization; 
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
#endif

namespace LendFoundry.DrawDownProcessor.Api {
    public class Startup {
        public void ConfigureServices (IServiceCollection services) {
#if DOTNET2
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new Info {
                    Version = "v1",
                        Title = "drawdown-processor"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "LendFoundry.DrawDownProcessor.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();

#else
            services.AddSwaggerDocumentation ();
#endif
            //services.AddSwaggerDocumentation();

            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddTenantTime ();
            services.AddAmortization();
            
            services.AddConfigurationService<DrawDownProcessorConfiguration> (Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            
            services.AddEventHub (Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService (Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddLookupService (Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddProductService (Settings.Product.Host, Settings.Product.Port);
            services.AddStatusManagementService (Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddDataAttributes (Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddDrawDown (Settings.DrawDown.Host, Settings.DrawDown.Port);
            //services.AddPersonalReportService(Settings.Experian.Host, Settings.Experian.Port);
            services.AddProductRuleService (Settings.ProductRule.Host, Settings.ProductRule.Port);
            //services.AddConsentService(Settings.Consent.Host, Settings.Consent.Port);
            services.AddFundingService (Settings.businessFunding.Host, Settings.businessFunding.Port);
            services.AddApplicationService (Settings.Application.Host, Settings.Application.Port);
            services.AddLoanManagementService (Settings.LoanManagement.Host, Settings.LoanManagement.Port);
            services.AddAchService (Settings.Ach.Host, Settings.Ach.Port);
            services.AddCashflowService (Settings.Cashflow.Host, Settings.Cashflow.Port);
            services.AddCalendarService (Settings.Calendar.Host, Settings.Calendar.Port);
            services.AddOfferEngineService (Settings.OfferEngine.Host, Settings.OfferEngine.Port);
            services.AddDatamerchService (Settings.DataMerchSyndication.Host, Settings.DataMerchSyndication.Port);
            services.AddTokenGeneratorService (Settings.TokenGenerator.Host, Settings.TokenGenerator.Port);
            services.AddCreditReportService (Settings.EquifaxCreditReport.Host, Settings.EquifaxCreditReport.Port);
            services.AddApplicantService (Settings.Applicant.Host, Settings.Applicant.Port);
            services.AddDocumentGenerator (Settings.DocumentGenerator.Host, Settings.DocumentGenerator.Port);
            services.AddApplicantDocumentService (Settings.ApplicationDocument.Host, Settings.ApplicationDocument.Port);
            services.AddAccrualBalance (Settings.LoanAccounting.Host, Settings.LoanAccounting.Port);
            services.AddVerificationEngineService (Settings.VerificationEngine.Host, Settings.VerificationEngine.Port);
            services.AddDocuSignService (Settings.DocuSign.Host, Settings.DocuSign.Port);
 
            services.AddTransient<DrawDownProcessorConfiguration> (p => p.GetService<IConfigurationService<DrawDownProcessorConfiguration>> ().Get ());
            services.AddSingleton<IMongoConfiguration> (p => new MongoConfiguration (Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IDrawDownProcessorService, DrawDownProcessorService> ();
            services.AddTransient<IDrawDownProcessorListener, DrawDownProcessorListener> ();
            services.AddTransient<IDrawDownProcessorServiceFactory, DrawDownProcessorServiceFactory> ();
            // aspnet mvc related
            services.AddMvc ().AddLendFoundryJsonOptions ();
            services.AddCors ();
        }

        public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
            app.UseHealthCheck ();
#if DOTNET2
            app.UseSwagger ();

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "DrawDown Processor Service");
            });
#else
            app.UseSwaggerDocumentation ();
#endif
            //Enable middleware to serve generated Swagger as a JSON endpoint.

            // app.UseSwaggerDocumentation();
            app.UseCors (env);
            app.UseErrorHandling ();
            app.UseRequestLogging ();
            //app.UseMiddleware<> ();
            app.UseMvc ();
            app.UseMiddleware<RequestLoggingMiddleware> ();
            app.UseDrawDownProcessorListener ();
        }
    }

    public class RequestLoggingMiddleware {
        private RequestDelegate Next { get; }

        public RequestLoggingMiddleware (RequestDelegate next) {
            Next = next;
        }

        public async Task Invoke (HttpContext context) {
            //The MVC middleware reads the body stream before we do, which
            //means that the body stream's cursor would be positioned at the end.
            // We need to reset the body stream cursor position to zero in order
            // to read it. As the default body stream implementation does not
            // support seeking, we need to explicitly enable rewinding of the stream:
            try {
                await Next.Invoke (context);

            } catch (Exception exception) 
            {
                Console.WriteLine (exception.Message.ToString ());
            }
        }
    }
}