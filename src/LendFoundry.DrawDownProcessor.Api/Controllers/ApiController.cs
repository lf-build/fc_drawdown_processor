﻿using CreditExchange.StatusManagement;
using LendFoundry.Foundation.Services;
using LendFoundry.Cashflow;
using LendFoundry.DrawDown;
using LendFoundry.ProductRule;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;

namespace LendFoundry.DrawDownProcessor.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IDrawDownProcessorService service)
        {
            Service = service;
        }

        private IDrawDownProcessorService Service { get; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawDownAddRequest"></param>
        /// <returns></returns>

#if DOTNET2
        [ProducesResponseType(typeof(IDrawDownResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        [HttpPost("")]
        public async Task<IActionResult> AddDrawDown([FromBody]DrawDownAddRequest drawDownAddRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                   return Ok(await Service.AddDrawDown(drawDownAddRequest));
                    //return Ok(drawdown);
                }
                catch(PreDrawdownEligiblityException ex)
                {
                    return new BadRequestObjectResult(ex.Message);
                }
            });
            //return await ExecuteAsync(async () => Ok(await Service.AddDrawDown(drawDownAddRequest)));
        }

#if DOTNET2
        [ProducesResponseType(typeof(IDrawDown), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        [HttpPut("drawDownId")]
        public async Task<IActionResult> UpdateDrawDown(string drawDownId,[FromBody]DrawDownAddRequest drawDownAddRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Service.UpdateDrawDown(drawDownId,drawDownAddRequest));
                    //return Ok(drawdown);
                }
                catch (PreDrawdownEligiblityException ex)
                {
                    return new BadRequestObjectResult(ex.Message);
                }
            });
            //return await ExecuteAsync(async () => Ok(await Service.AddDrawDown(drawDownAddRequest)));
        }

#if DOTNET2
        [ProducesResponseType(typeof(IProductRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        [HttpGet("{entityid}/compute/offer")]
        public async Task<IActionResult> ComputeOffer(string entityId)
        {
            return await ExecuteAsync(async () => Ok(await Service.ComputeOffer(entityId)));
        }


#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        [HttpPut("{entityId}/{StatusWorkFlowId}/approve")]
        public async Task<IActionResult> ApproveDrawDown(string entityId, string statusWorkFlowId, [FromBody]RequestModel reasons)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.ApproveDrawDown(entityId, statusWorkFlowId,reasons);
                return Ok();
            });
        }


#if DOTNET2
        [ProducesResponseType(typeof(IDrawDown), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        [HttpPut("{drawDownId}/drawamount")]
        public async Task<IActionResult> UpdateAmount(string drawDownId, [FromBody]DrawDownAddRequest drawDownRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Service.UpdateDrawDown(drawDownId, drawDownRequest));
                    //return Ok(drawdowndrawDownRequest
                }
                catch (PreDrawdownEligiblityException ex)
                {
                    return new BadRequestObjectResult(ex.Message);
                }
            });
        }


#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        [HttpPost("{drawdownId}/generate/agreement")]
        public async Task<IActionResult> GenerateLocAgreement(string drawdownId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    await Service.GenerateLocAgreement(drawdownId);
                    return Ok();
                }
                catch (IncompleteChecklistException ex)
                {
                    return new BadRequestObjectResult(ex.Message);
                }
            });
        }


#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        [HttpPost("{drawdownId}/send/agreement")]
        public async Task<IActionResult> SendLoanAgreement(string drawdownId, [FromBody]SendAgreementRequest returnUrl)
        {
            return
                await ExecuteAsync(
                    async () =>
                    {
                        await Service.SendToBorrower(drawdownId, returnUrl.returnUrl);
                        return Ok();
                    });
        }


#if DOTNET2
        [ProducesResponseType(typeof(DocusignReturnObject), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        [HttpPost("{drawdownId}/get/agreement")]
        public async Task<IActionResult> GetAgreementDocument(string drawdownId, [FromBody]string returnUrl)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await Service.GetAgreementDocument(drawdownId, returnUrl)));
        }


#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        [HttpPost("{entityid}/add/fundingrequest/{isResubmit?}")]
        public async Task<IActionResult> AddFundingRequest(string entityid, bool isResubmit, [FromBody]BankAccount bankAccountDetail)
        {
            return await ExecuteAsync(async () =>
            {
            try
            {
                await Service.AddFundingRequest(entityid, bankAccountDetail, isResubmit);
                return Ok();
                }
                catch (IncompleteChecklistException ex)
                {
                    return new BadRequestObjectResult(ex.Message);
                }
            });
        }


#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        [HttpPost("{entityid}/{status}/funding")]
        public async Task<IActionResult> UpdateFundingRequest(string entityid, string status)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.UpdateFundingStatus(entityid, status);
                return Ok();
            });
        }
    }
}