﻿using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.DrawDownProcessor.Client {
    [Obsolete ("Need to use the overloaded with Uri")]
    public static class DrawDownProcessorClientExtensions {
        /* public static IServiceCollection AddDrawDownProcessor (this IServiceCollection services, string endpoint, int port) {
             services.AddTransient<IDrawDownProcessorClientFactory> (p => new DrawDownProcessorClientFactory (p, endpoint, port));
             services.AddTransient (p => p.GetService<IDrawDownProcessorClientFactory> ().Create (p.GetService<ITokenReader> ()));
             return services;
         }

         public static IServiceCollection AddDrawDownProcessor (this IServiceCollection services, Uri uri) {
             services.AddSingleton<IApplicationServiceClientFactory> (p => new DrawDownProcessorClientFactory (p, uri));
             services.AddSingleton (p => p.GetService<IDrawDownProcessorClientFactory> ().Create (p.GetService<ITokenReader> ()));
             return services;
         }

         public static IServiceCollection AddDrawDownProcessor (this IServiceCollection services) {
             services.AddSingleton<IApplicationServiceClientFactory> (p => new DrawDownProcessorClientFactory (p));
             services.AddSingleton (p => p.GetService<IDrawDownProcessorClientFactory> ().Create (p.GetService<ITokenReader> ()));
             return services;
         }*/

        [Obsolete ("Need to use the overloaded with Uri")]
        public static IServiceCollection AddDrawDownProcessor (this IServiceCollection services, string endpoint, int port = 5000) {
            services.AddTransient<IDrawDownProcessorClientFactory> (p => new DrawDownProcessorClientFactory (p, endpoint, port));
            services.AddTransient (p => p.GetService<IDrawDownProcessorClientFactory> ().Create (p.GetService<ITokenReader> ()));
            return services;
        }
        public static IServiceCollection AddDrawDownProcessor (this IServiceCollection services, Uri uri) {
            services.AddSingleton<IDrawDownProcessorClientFactory> (p => new DrawDownProcessorClientFactory (p, uri));
            services.AddSingleton (p => p.GetService<IDrawDownProcessorClientFactory> ().Create (p.GetService<ITokenReader> ()));
            return services;
        }

        public static IServiceCollection AddDrawDownProcessor (this IServiceCollection services) {
            services.AddSingleton<IDrawDownProcessorClientFactory> (p => new DrawDownProcessorClientFactory (p));
            services.AddSingleton (p => p.GetService<IDrawDownProcessorClientFactory> ().Create (p.GetService<ITokenReader> ()));
            return services;
        }

    }
}