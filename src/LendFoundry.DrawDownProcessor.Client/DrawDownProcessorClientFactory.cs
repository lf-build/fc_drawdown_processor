﻿using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;

namespace LendFoundry.DrawDownProcessor.Client
{
    public class DrawDownProcessorClientFactory : IDrawDownProcessorClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public DrawDownProcessorClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        public DrawDownProcessorClientFactory(IServiceProvider provider, Uri uri = null, int cachingExpirationInSeconds = 60)
        {
            Provider = provider;
            Uri = uri;
            CachingExpirationInSeconds = cachingExpirationInSeconds;
        }
    private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }
        private int CachingExpirationInSeconds { get; }
        private Uri Uri { get; }

        public IDrawDownProcessorService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new DrawDownProcessorClient(client);
        }
    }
}
