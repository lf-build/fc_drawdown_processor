﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditExchange.StatusManagement;
using LendFoundry.Cashflow;
using LendFoundry.DrawDown;
using LendFoundry.Foundation.Client;
using LendFoundry.ProductRule;
using RestSharp;

namespace LendFoundry.DrawDownProcessor.Client {
    public class DrawDownProcessorClient : IDrawDownProcessorService {
        public DrawDownProcessorClient (IServiceClient client) {
            Client = client;
        }

        private IServiceClient Client { get; }

        public Task ACHFailStatusHandler (string entityType, string entityId, object data) {
            throw new NotImplementedException ();
        }

        public Task ACHStatusHandler (string entityType, string entityId, object data) {
            throw new NotImplementedException ();
        }

        public Task<IDrawDownResponse> AddDrawDown (IDrawDownAddRequest drawDownRequest) {
            throw new NotImplementedException ();
        }

        public Task AddFundingRequest (string drawDownId, IBankAccount bankAccountDetail, bool isResubmit = false) {
            throw new NotImplementedException ();
        }

        public Task ApproveDrawDown (string drawDownId, string statusWorkFlowId, IRequestModel reasons) {
            throw new NotImplementedException ();
        }

        public Task<IProductRuleResult> ComputeOffer (string entityId) {
            throw new NotImplementedException ();
        }

        public Task FundingStatusHandler (string entityType, string entityId, object data) {
            throw new NotImplementedException ();
        }

        public Task GenerateLocAgreement (string drawDownId) {
            throw new NotImplementedException ();
        }

        public Task<DocusignReturnObject> GetAgreementDocument (string drawDownId, string docusignReturnUrl) {
            throw new NotImplementedException ();
        }

        public Task SendToBorrower (string drawDownId, string docusignReturnUrl) {
            throw new NotImplementedException ();
        }

        public Task<bool> SignDocumentReceivedHandler (string entityType, string entityId, object data) {
            throw new NotImplementedException ();
        }

        public Task<IDrawDown> UpdateDrawDown (string drawDownId, IDrawDownAddRequest drawDownRequest) {
            throw new NotImplementedException ();
        }

        public Task UpdateFundingStatus (string entityId, string Status) {
            throw new NotImplementedException ();
        }
    }
}