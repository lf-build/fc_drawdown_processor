﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application;
using LendFoundry.Business.OfferEngine;
using LMS.Foundation.Amortization;
namespace LendFoundry.DrawDownProcessor {
    public class LocLoanAgreementDocument {
        public LocLoanAgreementDocument (IApplicant applicant, IDealOffer deal, IApplication application, dynamic data, double drawdownAmount, double percentage,List<AmortizationResponse> amortizationResponse,double prevOutstanding,double totalOutStanding,double totalPaybackAmount,int term,int monthdays,int monthweek,double mostRecentPurchasePrice) {
            this.MerchantLegalName = applicant.LegalBusinessName;
            this.DbaName = applicant.DBA;
            this.PhysicalAdd = $" { application.PrimaryAddress.AddressLine1} { application.PrimaryAddress.AddressLine2} { application.PrimaryAddress.AddressLine3} { application.PrimaryAddress.AddressLine4} {application.PrimaryAddress.City}, {application.PrimaryAddress.State}, {application.PrimaryAddress.ZipCode}";
            this.EntityType = applicant.BusinessType;
            this.SSN = $"{applicant.BusinessTaxID.Substring(0, 2)}-{applicant.BusinessTaxID.Substring(2, 7)}";
            this.State = application.PrimaryAddress.State;
            this.City = application.PrimaryAddress.City;
            this.Zip = application.PrimaryAddress.ZipCode;

            this.OwnerList = new List<OwnerDetails> ();
            var PrimaryOwner = applicant.Owners?.FirstOrDefault (x => x.IsPrimary == true);
            var owner = new OwnerDetails () {
                id = 1,
                OwnerName = PrimaryOwner.FirstName + " " + PrimaryOwner.LastName,
                OwnerAdd = $"{ PrimaryOwner.Addresses[0].AddressLine1} { PrimaryOwner.Addresses[0].AddressLine2} { PrimaryOwner.Addresses[0].AddressLine3} { PrimaryOwner.Addresses[0].AddressLine4}{ PrimaryOwner.Addresses[0].City}, {PrimaryOwner.Addresses[0].State}, {PrimaryOwner.Addresses[0].ZipCode}"
            };

            Owner1Name = owner.OwnerName;
            this.OwnerList.Add (owner);
            var secOwners = applicant.Owners?.Where (x => x.IsPrimary != true).ToList ();
            for (int i = 0; i < secOwners.Count; i++) {
                var obj = new OwnerDetails () {
                    id = i + 2,
                    OwnerName = secOwners[i].FirstName + " " + secOwners[i].LastName,
                    OwnerAdd = $"{ secOwners[i].Addresses[0].AddressLine1} { secOwners[i].Addresses[0].AddressLine2} { secOwners[i].Addresses[0].AddressLine3} { secOwners[i].Addresses[0].AddressLine4}{ secOwners[i].Addresses[0].City}, {secOwners[i].Addresses[0].State}, {secOwners[i].Addresses[0].ZipCode}"
                };
                this.OwnerList.Add (obj);
            }

            this.FundingAmount = Formatter.MoneyFormatter (drawdownAmount);
            this.ProcessingFee = Formatter.MoneyFormatter ((percentage) * drawdownAmount);
            this.Disbursment = Formatter.MoneyFormatter ((drawdownAmount - (percentage) * drawdownAmount));
            this.Frequency = deal.DurationType;
            var currDate = DateTime.Now;
            DateTimeFormatInfo dtf = new DateTimeFormatInfo ();
            this.DateCreated = $" {dtf.GetMonthName(currDate.Month)} {currDate.Day.ToString()}, {currDate.Year.ToString()}";
            this.BusineesStartDate = applicant.BusinessStartDate.ToString ();
            this.TotalPayback = Formatter.MoneyFormatter ((drawdownAmount * deal.SellRate));
            this.MonthlyPercentage = "15 %";
            this.Title = "Owner";
           // var loanSchedule = data.loanSchedule;
            /*if (loanSchedule != null) {
                this.DrawdownSchedule = new List<DrawdownSchedule> ();
                for (int i = 0; i < deal.Term; i++) {
                    this.DrawdownSchedule.Add (new DrawdownSchedule () {
                        MonthId = i + 1,
                            Principle = Formatter.MoneyFormatter (loanSchedule[i].PrincipalAmount),
                            Payment = Formatter.MoneyFormatter (loanSchedule[i].PaymentAmount),
                            Interest = Formatter.MoneyFormatter (loanSchedule[i].InterestAmount),
                            TotalPayback = Formatter.MoneyFormatter (loanSchedule[i].PaybackAmount)
                    });
                }
                this.DailyAmount = Formatter.MoneyFormatter (loanSchedule[0].PaymentAmount);
            }*/
            if (amortizationResponse != null) {
                this.DrawdownSchedule = new List<DrawdownSchedule> ();
                for (int i = 0; i < deal.Term; i++) {
                    this.DrawdownSchedule.Add (new DrawdownSchedule () {
                        MonthId = i + 1,
                           Principle = Formatter.MoneyFormatter (amortizationResponse[i].PrincipalAmount),
                            Payment = Formatter.MoneyFormatter (amortizationResponse[i].PaymentAmount),
                            Interest = Formatter.MoneyFormatter (amortizationResponse[i].InterestAmount),
                            TotalPayback = Formatter.MoneyFormatter (amortizationResponse[i].TotalPaybackAmount)

                            
                    });
                }
                this.DailyAmount = Formatter.MoneyFormatter (amortizationResponse[0].PaymentAmount);
            }
            if(amortizationResponse != null)
            {
                 if (deal.DurationType.ToLower () == "weekly") {
                    if (amortizationResponse.Count >= 1)
                        this.HigherOrder = Formatter.MoneyFormatter (Convert.ToDouble ((Math.Round (amortizationResponse[0].PaymentAmount / monthdays, 2)) * monthweek));
                    if (amortizationResponse.Count >= 3)
                        this.LowerOrder = Formatter.MoneyFormatter (Convert.ToDouble ((Math.Round (amortizationResponse[2].PaymentAmount / monthdays, 2)) * monthweek));

                } else {
                    if (amortizationResponse.Count >= 1)
                        this.HigherOrder = Formatter.MoneyFormatter (Convert.ToDouble ((Math.Round (amortizationResponse[0].PaymentAmount / monthdays, 2))));
                    if (amortizationResponse.Count >= 3)
                        this.LowerOrder = Formatter.MoneyFormatter (Convert.ToDouble ((Math.Round (amortizationResponse[2].PaymentAmount / monthdays, 2))));
                }
            }
            //this.LowerOrder = Formatter.MoneyFormatter (data.payOrders.lowerPay);
           // this.HigherOrder = Formatter.MoneyFormatter (data.payOrders.higherPay);
            this.RoutingNumber = deal.RoutingNumber;
            this.AccountNumber = deal.AccountNumber;
            this.DepositoryName = deal.AccountType;
            this.PrevOutstanding = Formatter.MoneyFormatter (prevOutstanding);
            this.TotalOutstanding = Formatter.MoneyFormatter (totalOutStanding);
            this.DrawdownAmount = Formatter.MoneyFormatter (drawdownAmount);;
            this.TotalPaybackAmount= Formatter.MoneyFormatter (totalPaybackAmount);
 
            this.Term = Convert.ToString(term);
            this.MostRecentPurchasePrice = Formatter.MoneyFormatter(mostRecentPurchasePrice);
        }
         
        public string MostRecentPurchasePrice {get;set;}
        public string PrevOutstanding {get;set;}
        public string TotalOutstanding {get;set;}
        public string DrawdownAmount {get;set;}
        public string TotalPaybackAmount {get;set;}

        public string MerchantLegalName { get; set; }
        public string DbaName { get; set; }
        public string PhysicalAdd { get; set; }
        public string EntityType { get; set; }
        public string SSN { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Owner1Name { get; set; }
        public string Owner1Add { get; set; }
        public string Owner2Name { get; set; }
        public string Owner2Add { get; set; }
        public string FundingAmount { get; set; }
        public string MonthlyPercentage { get; set; }
        public string DailyAmount { get; set; }
        public string TotalPayback { get; set; }
        public string ProcessingFee { get; set; }
        public string Disbursment { get; set; }
        public string DateCreated { get; set; }
        public string Frequency { get; set; }
        public string BusineesStartDate { get; set; }
        public string Title { get; set; }
        public List<DrawdownSchedule> DrawdownSchedule { get; set; }
        public dynamic LowerOrder { get; set; }
        public dynamic HigherOrder { get; set; }
        public string LenderName { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string DepositoryName { get; set; }
        public List<OwnerDetails> OwnerList { get; set; }
        public string Logo { get; set; }
        public string Term { get; set; }
    }
}