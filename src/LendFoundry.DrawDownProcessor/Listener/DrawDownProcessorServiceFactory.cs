﻿using System;
using CreditExchange.Datamerch.Client;
using CreditExchange.StatusManagement.Client;
using LendFoundry.Application.Document.Client;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Business.Funding.Client;
using LendFoundry.Business.OfferEngine.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Cashflow.Client;
 
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocuSign.Client;
using LendFoundry.DrawDown;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Equifax.Client;
using LendFoundry.Token.Generator.Client;
using LendFoundry.VerificationEngine.Client;
using LMS.LoanAccounting;
using LMS.LoanManagement.Client;
using LendFoundry.Configuration;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.DrawDownProcessor {
    public class DrawDownProcessorServiceFactory : IDrawDownProcessorServiceFactory {
        public DrawDownProcessorServiceFactory (IServiceProvider provider) {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IDrawDownProcessorService Create (ITokenReader reader) {
            var eventHubServiceFactory = Provider.GetService<IEventHubClientFactory> ();
            var eventHubService = eventHubServiceFactory.Create (reader);
   
            var configurationServiceFactory = Provider.GetService<LendFoundry.Configuration.IConfigurationServiceFactory> ();
            var drawDownProcessorConfigurationService = configurationServiceFactory.Create<DrawDownProcessorConfiguration> (Settings.ServiceName, reader);
            var drawDownProcessorConfiguration = drawDownProcessorConfigurationService.Get ();

            var applicationServiceClientFactory = Provider.GetService<IApplicationServiceClientFactory> ();
            var applicationService = applicationServiceClientFactory.Create (reader);

            var drawDownClientFactory = Provider.GetService<IDrawDownClientFactory> ();
            var drawdown = drawDownClientFactory.Create (reader);

            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory> ();
            var dataAttributesService = dataAttributesClientFactory.Create (reader);

            var lookUpFactory = Provider.GetService<ILookupClientFactory> ();
            var lookUpService = lookUpFactory.Create (reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory> ();
            var statusManagementService = statusManagementServiceFactory.Create (reader);

            var loanManagementServiceFactory = Provider.GetService<ILoanManagementClientServiceFactory> ();
            var loanManagementService = loanManagementServiceFactory.Create (reader);

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory> ();
            var tenantTime = tenantTimeFactory.Create (configurationServiceFactory, reader);

            var productRuleClientServiceFactory = Provider.GetService<IProductRuleServiceClientFactory> ();
            var productRuleClientService = productRuleClientServiceFactory.Create (reader);

            var fundingServiceFactory = Provider.GetService<IFundingServiceClientFactory> ();
            var fundingService = fundingServiceFactory.Create (reader);

            var queueServiceFactory = Provider.GetService<IAchServiceFactory> ();
            var queueService = queueServiceFactory.Create (reader);

            var cashFlowServiceFactory = Provider.GetService<ICashflowClientFactory> ();
            var cashFlowService = cashFlowServiceFactory.Create (reader);

            var calendarServiceFactory = Provider.GetService<ICalendarServiceFactory> ();
            var calendar = calendarServiceFactory.Create (reader);

            var offerEngineServiceClientFactory = Provider.GetService<IOfferEngineServiceClientFactory> ();
            var offerEngineService = offerEngineServiceClientFactory.Create (reader);

            var creditReportClientFactory = Provider.GetService<ICreditReportClientFactory> ();
            var creditReportService = creditReportClientFactory.Create (reader);

            var datamerchServiceClientFactory = Provider.GetService<IDatamerchServiceClientFactory> ();
            var datamerchService = datamerchServiceClientFactory.Create (reader);

            var tokenGeneratorServiceClientFactory = Provider.GetService<ITokenGeneratorServiceFactory> ();
            var tokenGeneratorService = tokenGeneratorServiceClientFactory.Create (reader);

            var applicantServiceClientFactory = Provider.GetService<IApplicantServiceClientFactory> ();
            var applicantService = applicantServiceClientFactory.Create (reader);

            var documentGeneratorServiceFactory = Provider.GetService<IDocumentGeneratorServiceFactory> ();
            var documentGeneratorService = documentGeneratorServiceFactory.Create (reader);

            var applicationDocumentServiceClientFactory = Provider.GetService<IApplicationDocumentServiceClientFactory> ();
            var applicationDocumentService = applicationDocumentServiceClientFactory.Create (reader);

            var accrualBalanceServiceClientFactory = Provider.GetService<IAccrualBalanceClientFactory> ();
            var accrualBalanceService = accrualBalanceServiceClientFactory.Create (reader);

            var verificationServiceFactory = Provider.GetService<IVerificationEngineServiceClientFactory> ();
            var verificationService = verificationServiceFactory.Create (reader);

            var docuSignServiceClientFactory = Provider.GetService<IDocuSignServiceClientFactory> ();
            var docuSignService = docuSignServiceClientFactory.Create (reader);

            return new DrawDownProcessorService (drawdown, eventHubService, statusManagementService, drawDownProcessorConfiguration, dataAttributesService,
                applicationService, loanManagementService, offerEngineService, productRuleClientService,
                tenantTime, cashFlowService, queueService, creditReportService,
                datamerchService, tokenGeneratorService, applicantService, accrualBalanceService,
                documentGeneratorService, applicationDocumentService, verificationService, lookUpService, docuSignService, fundingService,null);
        }
    }
}