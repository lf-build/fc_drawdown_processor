﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Reflection;

namespace LendFoundry.DrawDownProcessor
{
    public class DrawDownProcessorListener : IDrawDownProcessorListener
    {
        #region Constructor
        public DrawDownProcessorListener
        (
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            IDrawDownProcessorServiceFactory drawDownProcessorServiceFactory,
            IConfigurationServiceFactory configurationFactory)
        {

            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(drawDownProcessorServiceFactory), drawDownProcessorServiceFactory);
            EnsureParameter(nameof(tenantTimeFactory), tenantTimeFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);

            EventHubFactory = eventHubFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            DrawDownProcessorServiceFactory = drawDownProcessorServiceFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
        }
        #endregion

        #region Private Properties       
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IDrawDownProcessorServiceFactory DrawDownProcessorServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        #endregion

        #region Public Methods

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenantId = Environment.GetEnvironmentVariable("TenantId");
                logger.Info($"Processing tenant #{tenantId}");
                var token = TokenHandler.Issue(tenantId, Settings.ServiceName, null, "system", null);
                var reader = new StaticTokenReader(token.Value);
                var eventHub = EventHubFactory.Create(reader);
                var configuration = ConfigurationFactory.Create<DrawDownProcessorConfiguration>(Settings.ServiceName, reader).Get();

                if (configuration != null && configuration.events != null)
                {
                    var drawDownProcessorService = DrawDownProcessorServiceFactory.Create(reader);
                    configuration
                   .events
                   .ToList().ForEach(events =>
                   {
                       eventHub.On(events.Name, ProcessEvent(events, logger, drawDownProcessorService));
                       logger.Info($"It was made subscription to EventHub with the Event: #{events.Name}");
                   });

                    logger.Info("-------------------------------------------");
                }
                else
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                }
               // eventHub.StartAsync();

                logger.Info("DrawDown Processor listener started");
            }
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub to process DrawDown Processor listener", ex);
                logger.Info("\n DrawDown Processor listener  is working yet and waiting new event\n");
                //Start();
            }
        }

        #endregion

        #region Private Methods

        #region Process Event
        private Action<LendFoundry.EventHub.EventInfo> ProcessEvent(
           EventConfiguration eventConfiguration,
           ILogger logger,
           IDrawDownProcessorService drawDownProcessorClientService
           )
        {
            return async @event =>
            {
                string responseData = string.Empty;
                object data = null;

                try
                {
                    try
                    {
                        responseData = eventConfiguration.Response.FormatWith(@event); //posted data
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);
                    }
                    catch { }

                    var entityId = eventConfiguration.EntityId.FormatWith(@event);
                    var entityType = "drawdown";
                    var Name = eventConfiguration.Name.FormatWith(@event); //Application Processor event name

                    var methodName = eventConfiguration.MethodToExecute;
                    MethodInfo method = typeof(IDrawDownProcessorService).GetMethod(methodName);
                    object[] param = { entityType, entityId, data };
                    await (dynamic)method.Invoke(drawDownProcessorClientService, param);

                    logger.Info($"Entity Id: #{entityId}");
                    logger.Info($"Response Data : #{data}");
                    logger.Info($"DrawDown Processor event {Name} completed for {entityId} ");
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhadled exception while listening event {@event.Name}", ex);
                }
            };
        }
        #endregion

        #region Ensure Parameter
        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException($"{name} cannot be null.");
        }
        #endregion

        #region Execute Request
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        #endregion

        #endregion

    }
}
