﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.DrawDownProcessor
{
    public interface IDrawDownProcessorServiceFactory
    {
        IDrawDownProcessorService Create(ITokenReader reader);
    }
}
