﻿namespace LendFoundry.DrawDownProcessor
{
    public interface IDrawDownProcessorListener
    {
        void Start();
    }
}