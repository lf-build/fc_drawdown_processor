﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.DrawDownProcessor
{
    public static class DrawDownProcessorListenerExtensions
    {
        public static void UseDrawDownProcessorListener(this IApplicationBuilder application)
        {
            application.ApplicationServices.GetRequiredService<IDrawDownProcessorListener>().Start();
        }
    }
}
