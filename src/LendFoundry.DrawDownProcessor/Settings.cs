﻿using System;
using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.DrawDownProcessor {
    public class Settings {
        private const string Prefix = "DRAW-DOWN-PROCESSOR";

        public static string ServiceName => Prefix.ToLower ();
    }
}