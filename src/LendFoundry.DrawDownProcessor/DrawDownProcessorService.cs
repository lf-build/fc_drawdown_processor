﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.StatusManagement;
using CreditExchange.Syndication.Datamerch;
using LendFoundry.Application.Document;
using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application;
using LendFoundry.Business.Funding;
using LendFoundry.Business.OfferEngine;
using LendFoundry.Cashflow;
using LendFoundry.DataAttributes;
using LendFoundry.DocumentGenerator;
using LendFoundry.DrawDown;
using LendFoundry.DrawDownProcessor.Events;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.ProductRule;
using LendFoundry.Syndication.Equifax;
using LendFoundry.Syndication.LFDocuSign;
using LendFoundry.Syndication.LFDocuSign.Models;
using LendFoundry.Syndication.LFDocuSign.Response;
using LendFoundry.TemplateManager;
using LendFoundry.Token.Generator;
using LendFoundry.VerificationEngine;
using LMS.Foundation.Amortization;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LMS.LoanManagement.Client;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LendFoundry.DrawDownProcessor {
    public class DrawDownProcessorService : IDrawDownProcessorService {

        #region Constructor

        public DrawDownProcessorService
            (
                IDrawDownService drawDownService,
                IEventHubClient eventhub,
                IEntityStatusService statusManagementService,
                DrawDownProcessorConfiguration drawDownConfiguration,
                IDataAttributesEngine dataAttributeEngine,
                IApplicationService businessApplicationService,
                ILoanManagementClientService loanManagementService,
                IOfferEngineService offerEngineService,
                IProductRuleService productRuleService,
                ITenantTime tenantTime,
                ICashflowService ibvService,
                IQueueService queueService,
                IEquifaxCreditReportService equifaxCreditReportService,
                IDataMerchService dataMerchService,
                ITokenGeneratorService tokenGeneratorService,
                IApplicantService businessApplicantService,
                IAccrualBalanceService accrualBalanceService,
                IDocumentGeneratorService documentGeneratorService,
                IApplicationDocumentService applicationDocumentService,
                IVerificationEngineService verificationEngineService,
                ILookupService lookupService,
                IDocuSignService docuSignService,
                IFundingService fundingService,
                ILoanAmortization amortization

            ) {
                if (businessApplicationService == null)
                    throw new ArgumentNullException (nameof (businessApplicationService));

                if (eventhub == null) throw new ArgumentException ($"{nameof(eventhub)} is mandatory");

                if (statusManagementService == null) throw new ArgumentException ($"{nameof(statusManagementService)} is mandatory");
                if (drawDownConfiguration == null) throw new ArgumentException ($"{nameof(drawDownConfiguration)} is mandatory");
                if (dataAttributeEngine == null) throw new ArgumentException ($"{nameof(dataAttributeEngine)} is mandatory");
                if (businessApplicationService == null) throw new ArgumentException ($"{nameof(businessApplicationService)} is mandatory");
                if (loanManagementService == null) throw new ArgumentException ($"{nameof(loanManagementService)} is mandatory");
                if (offerEngineService == null) throw new ArgumentException ($"{nameof(offerEngineService)} is mandatory");
                if (productRuleService == null) throw new ArgumentException ($"{nameof(productRuleService)} is mandatory");
                if (tenantTime == null) throw new ArgumentException ($"{nameof(tenantTime)} is mandatory");
                if (productRuleService == null) throw new ArgumentException ($"{nameof(productRuleService)} is mandatory");
                if (ibvService == null) throw new ArgumentException ($"{nameof(ibvService)} is mandatory");
                if (queueService == null) throw new ArgumentException ($"{nameof(queueService)} is mandatory");
                if (dataMerchService == null) throw new ArgumentException ($"{nameof(dataMerchService)} is mandatory");
                if (tokenGeneratorService == null) throw new ArgumentException ($"{nameof(tokenGeneratorService)} is mandatory");
                if (businessApplicantService == null) throw new ArgumentException ($"{nameof(businessApplicantService)} is mandatory");
                if (equifaxCreditReportService == null) throw new ArgumentException ($"{nameof(equifaxCreditReportService)} is mandatory");
                if (accrualBalanceService == null) throw new ArgumentException ($"{nameof(accrualBalanceService)} is mandatory");
                if (documentGeneratorService == null) throw new ArgumentException ($"{nameof(documentGeneratorService)} is mandatory");
                if (applicationDocumentService == null) throw new ArgumentException ($"{nameof(applicationDocumentService)} is mandatory");
                if (verificationEngineService == null) throw new ArgumentException ($"{nameof(verificationEngineService)} is mandatory");
                if (lookupService == null) throw new ArgumentException ($"{nameof(lookupService)} is mandatory");
                if (docuSignService == null) throw new ArgumentException ($"{nameof(docuSignService)} is mandatory");
                if (fundingService == null) throw new ArgumentException ($"{nameof(fundingService)} is mandatory");
                DrawDownService = drawDownService;
                EventHub = eventhub;
                StatusManagementService = statusManagementService;
                DrawDownConfiguration = drawDownConfiguration;
                DataAttributeEngine = dataAttributeEngine;
                BusinessApplicationService = businessApplicationService;
                LoanManagementService = loanManagementService;
                OfferEngineService = offerEngineService;
                ProductRuleService = productRuleService;
                TenantTime = tenantTime;
                IbvService = ibvService;
                QueueService = queueService;
                DataMerchService = dataMerchService;
                TokenGeneratorService = tokenGeneratorService;
                BusinessApplicantService = businessApplicantService;
                EquifaxCreditReportService = equifaxCreditReportService;
                AccrualBalanceService = accrualBalanceService;
                DocumentGeneratorService = documentGeneratorService;
                ApplicationDocumentService = applicationDocumentService;
                VerificationEngineService = verificationEngineService;
                LookupService = lookupService;
                DocuSignService = docuSignService;
                FundingService = fundingService;
                 LoanAmortization = amortization;
            }

        #endregion

        #region Variables
   private ILoanAmortization LoanAmortization { get; }
        private IDataAttributesEngine DataAttributeEngine { get; set; }
        private DrawDownProcessorConfiguration DrawDownConfiguration { get; }
        private IEntityStatusService StatusManagementService { get; set; }
        private IDrawDownService DrawDownService { get; }
        private IApplicationService BusinessApplicationService { get; }
        private IEventHubClient EventHub { get; }
        private IProductRuleService ProductRuleService { get; set; }
        private ILoanManagementClientService LoanManagementService { get; }
        private ITenantTime TenantTime { get; }
        private IOfferEngineService OfferEngineService { get; }
        private ICashflowService IbvService { get; }
        private IQueueService QueueService { get; }
        private IEquifaxCreditReportService EquifaxCreditReportService { get; }

        private IDataMerchService DataMerchService { get; }

        private ITokenGeneratorService TokenGeneratorService { get; }
        private IApplicantService BusinessApplicantService { get; }

        private IAccrualBalanceService AccrualBalanceService { get; }

        private IDocumentGeneratorService DocumentGeneratorService { get; }
        private IApplicationDocumentService ApplicationDocumentService { get; }
        private IVerificationEngineService VerificationEngineService { get; }
        private ILookupService LookupService { get; }

        private IDocuSignService DocuSignService { get; }

        private IFundingService FundingService { get; }

        private string EntityType { get; set; } = "drawdown";
        // private double WeekConstant = 4.34812141;

        #endregion

        public async Task<IDrawDownResponse> AddDrawDown (IDrawDownAddRequest drawDownRequest) {
            if (drawDownRequest == null)
                throw new ArgumentException ($"{nameof(drawDownRequest)} is mandatory");

            if (string.IsNullOrWhiteSpace (drawDownRequest.LocNumber))
                throw new ArgumentException ($"{nameof(drawDownRequest.LocNumber)} is mandatory");

            if (drawDownRequest.RequestedAmount < 0)
                throw new ArgumentException ($"{nameof(drawDownRequest.RequestedAmount)} is mandatory");

            if (drawDownRequest.RequestedAmount > 0) {
                var dealData = await OfferEngineService.GetApplicationOffers ("application", drawDownRequest.LocNumber);
                var payload = new { applicationNumber = drawDownRequest.LocNumber, amount = drawDownRequest.RequestedAmount, dealData = dealData.DealOffer };
                string productId = await GetProductId ("application", drawDownRequest.LocNumber);

                var isDrawEligible = await ProductRuleService.RunRule ("application", drawDownRequest.LocNumber, productId, "DrawDownAmountEligibility", payload);

                if (isDrawEligible.Result == Result.Failed) {
                    throw new PreDrawdownEligiblityException (isDrawEligible.ResultDetail[0]);
                }
            }

            var drawDownAddRequest = GetDrawDownRequest (drawDownRequest);
            var drawDown = await DrawDownService.Add (drawDownAddRequest);

            if (drawDown == null || drawDown.DrawDown == null || string.IsNullOrEmpty (drawDown.DrawDown.EntityId)) {
                throw new Exception ("Drawdown initiation failed");
            }

            var isEligible = await ProductRuleService.RunPricingStrategy (EntityType, drawDown.DrawDown.EntityId, drawDown.ProductId, "DrawDownEligibility", "1.0");
            //CashflowService.GetBankAccountDetails("application", drawDown.DrawDown.LocNumber,)
            if (isEligible?.Result == Result.Passed) {
                var applicationData = await BusinessApplicationService.GetByApplicationNumber (drawDownRequest.LocNumber);
                var applicantData = await BusinessApplicantService.Get (applicationData.ApplicantId);
                var applicationStatusDetail = await StatusManagementService.GetActiveStatusWorkFlow ("application", applicationData.ApplicationNumber);
                //SaveTermofUseConsent(drawDown.DrawDown.EntityId, drawDown.ProductId);//For IDF consent is not rquired
                var drawDownReq = new UpdateDrawDownAmountRequest ();
                drawDownReq.DrawDownAmount = applicationData.TotalDrawDownAmount + drawDownRequest.RequestedAmount;
                await SyndicationCall (drawDown.DrawDown.EntityId, applicationData, applicantData);

                await StatusManagementService.ChangeStatus (EntityType, drawDown.DrawDown.EntityId, drawDown.StatusWorkFlowId, DrawDownConfiguration.Statuses["Verification"], new RequestModel ());

            } else {
                List<string> reasons = new List<string> ();
                reasons.Add ("EligibilityFailed");
                var requestModel = new RequestModel ();
                requestModel.reasons = reasons;
                await StatusManagementService.ChangeStatus (EntityType, drawDown.DrawDown.EntityId, drawDown.StatusWorkFlowId, DrawDownConfiguration.Statuses["Rejected"], requestModel);
            }
            await EventHub.Publish (new DrawDownProcessing {
                EntityId = drawDown.DrawDown.EntityId,
                    EntityType = EntityType,
                    Response = null,
                    Request = "drawdown",
                    ReferenceNumber = Guid.NewGuid ().ToString ("N")
            });
            return drawDown;
        }
        public async Task<IDrawDown> UpdateDrawDown (string drawDownId, IDrawDownAddRequest drawDownRequest) {
            EnsureInputIsValid (drawDownId, drawDownRequest);

            var applicationData = await BusinessApplicationService.GetByApplicationNumber (drawDownRequest.LocNumber);
            var drawDownReq = new UpdateDrawDownAmountRequest ();
            drawDownReq.DrawDownAmount = applicationData.TotalDrawDownAmount + drawDownRequest.RequestedAmount;

            var statusDetail = await StatusManagementService.GetActiveStatusWorkFlow ("drawdown", drawDownId);
            if (statusDetail == null)
                throw new InvalidOperationException ($"Drawdown status detail not found for drawdown number : {drawDownId}");

            //if (updatedDrawdown.RequestedAmount == drawDownRequest.RequestedAmount)
            //{
            var updatedDrawdown = await ApproveDrawDown (drawDownId, statusDetail.StatusWorkFlowId, drawDownRequest.RequestedAmount, new RequestModel ());

            return updatedDrawdown;
        }

        private static void EnsureInputIsValid (string drawDownId, IDrawDownAddRequest drawDownRequest) {
            if (string.IsNullOrWhiteSpace (drawDownId))
                throw new ArgumentException ($"{nameof(drawDownId)} is mandatory");

            if (drawDownRequest == null)
                throw new ArgumentException ($"{nameof(drawDownRequest)} is mandatory");

            if (string.IsNullOrWhiteSpace (drawDownRequest.LocNumber))
                throw new ArgumentException ($"{nameof(drawDownRequest.LocNumber)} is mandatory");

            if (drawDownRequest.RequestedAmount <= 0)
                throw new ArgumentException ($"{nameof(drawDownRequest.RequestedAmount)} is mandatory");
        }

        public async Task ApproveDrawDown (string drawDownId, string statusWorkFlowId, IRequestModel reasons) {
            var drawDown = await DrawDownService.GetByEntityId (drawDownId);
            if (drawDown == null)
                throw new NotFoundException ($"No drawdown found for {drawDownId}");

            await ApproveDrawDown (drawDownId, statusWorkFlowId, drawDown.RequestedAmount, reasons);
        }

        private async Task<IDrawDown> ApproveDrawDown (string drawDownId, string statusWorkFlowId, double amount, IRequestModel reasons) {
            if (string.IsNullOrWhiteSpace (drawDownId))
                throw new ArgumentNullException (nameof (drawDownId));

            if (string.IsNullOrWhiteSpace (statusWorkFlowId))
                throw new ArgumentNullException (nameof (statusWorkFlowId));

            var reason = new RequestModel ();
            if (reasons != null) {
                reason.Note = reasons.Note;
            }

            var drawDown = await DrawDownService.GetByEntityId (drawDownId);

            if (drawDown == null)
                throw new NotFoundException ($"No drawdown found for {drawDownId}");
            //if (isDrawEligible.Result == Result.Failed)
            //{
            //    throw new PreDrawdownEligiblityException(isDrawEligible.ResultDetail[0]);
            //}

            var dealData = await OfferEngineService.GetApplicationOffers ("application", drawDown.LocNumber);
            var payload = new { applicationNumber = drawDown.LocNumber, amount = amount, dealData = dealData.DealOffer };
            string productId = await GetProductId ("application", drawDown.LocNumber);

            var isDrawEligible = await ProductRuleService.RunRule ("application", drawDown.LocNumber, productId, "DrawDownAmountEligibility", payload);
            if (isDrawEligible.Result == Result.Failed) {
                throw new PreDrawdownEligiblityException (isDrawEligible.ResultDetail.Count > 0 ? isDrawEligible.ResultDetail[0] : string.Empty);
            }
            //var updatedDrawdown = await DrawDownService.UpdateDrawdownAmount(drawDownId, new DrawDownRequest { LocNumber = drawDown.LocNumber, RequestedAmount = amount });

            var statusDetail = await StatusManagementService.GetActiveStatusWorkFlow ("application", drawDown.LocNumber);
            if (statusDetail == null)
                throw new InvalidOperationException ($"Application status detail not found for application number : {drawDown.LocNumber}");

            if (statusDetail.Code == DrawDownConfiguration.Statuses["LoCOpen"]) {

                var applicationData = await BusinessApplicationService.GetByApplicationNumber (drawDown.LocNumber);
                var applicationStatusDetail = await StatusManagementService.GetActiveStatusWorkFlow ("application", applicationData.ApplicationNumber);

                await DrawDownService.UpdatedApprovalDetails (drawDownId, new ApprovalDetailsRequest { ApprovedAmount = amount });

                await StatusManagementService.ChangeStatus ("drawdown", drawDownId, statusWorkFlowId, DrawDownConfiguration.Statuses["Approved"], reason);

                await EventHub.Publish ("DrawdownApproved", new {
                    EntityId = drawDown.EntityId,
                        ProductId = productId,
                        DrawDown = drawDown
                });
            } else {
                throw new InvalidOperationException ($"Drawdown can not be approved when application is not in open or hold");
            }
            return drawDown;
        }
        public async Task AddFundingRequest (string drawDownId, IBankAccount bankAccountDetail, bool isResubmit = false) {
            if (string.IsNullOrWhiteSpace (drawDownId))
                throw new ArgumentNullException (nameof (drawDownId));

            var drawDownDetail = await DrawDownService.GetByEntityId (drawDownId);

            if (drawDownDetail == null)
                throw new InvalidOperationException ("Application not found");

            var productId = await GetProductId (EntityType, drawDownId);
            var factDetails = await VerificationEngineService.GetFactVerification (EntityType, drawDownId, productId, false);

            if (factDetails != null && factDetails.Any ()) {
                var incompletFacts = factDetails.Where (fact => fact.VerificationStatus != VerificationEngine.VerificationStatus.Passed);
                if (incompletFacts != null && incompletFacts.Any ()) {
                    throw new IncompleteChecklistException ("Pending Verification");
                }
            }

            //var dealData = await DataAttributeEngine.GetAttribute("application", drawDownDetail.LocNumber, "SelectedDealoffer");
            var dealData = await OfferEngineService.GetApplicationOffers ("application", drawDownDetail.LocNumber);
            var payload = new { drawDown = drawDownDetail, selectedDealoffer = dealData };

            var drawdownFeeDetails = await ProductRuleService.RunRule (EntityType, drawDownId, productId, "CalculatePerDrawDownFee", payload);

            double drawDownFeeAmount = 0;
            if (drawdownFeeDetails.Result == Result.Passed) {
                JObject intermediateData = JObject.Parse (drawdownFeeDetails.IntermediateData.ToString ());
                drawDownFeeAmount = Convert.ToDouble (intermediateData["feeAmount"]);
                await DrawDownService.UpdatedFeeDetails (drawDownId, new FeeDetailsRequest () { DrawDownFeeAmount = drawDownFeeAmount });
            }

            var application = await BusinessApplicationService.GetByApplicationNumber (drawDownDetail.LocNumber);
            var ApplicationStatusDetail = await StatusManagementService.GetActiveStatusWorkFlow ("application", drawDownDetail.LocNumber);
            if (ApplicationStatusDetail == null)
                throw new InvalidOperationException ($"Application status detail not found for application number : {drawDownDetail.LocNumber}");

            if (ApplicationStatusDetail.Code == DrawDownConfiguration.Statuses["LoCOpen"] || ApplicationStatusDetail.Code == DrawDownConfiguration.Statuses["LoCHold"]) {
                var fundingAccount = dealData.DealOffer.AccountNumber;

                // var fundingAccount = fundingAccounts != null && fundingAccounts.accounts != null ? fundingAccounts.accounts.FirstOrDefault() : null;
                if (fundingAccount == null)
                    throw new InvalidOperationException ($"Funding account not found for application number : {drawDownDetail.LocNumber}");

                InstructionRequest instructionRequest = new InstructionRequest ();
                instructionRequest.Amount = Convert.ToDecimal (drawDownDetail.ApprovedAmount - drawDownFeeAmount);
                instructionRequest.EntryDescription = "";
                instructionRequest.Frequency = 0;
                instructionRequest.FundingSourceId = DrawDownConfiguration.FundingSourceId;
                Receiving receiving = new Receiving ();
                receiving.AccountNumber = dealData.DealOffer.AccountNumber; //GetAccountNumber((fundingAccount)[0]);
                receiving.AccountType = MoneyMovement.Ach.AccountType.Individual;
                receiving.Name = "Test";
                receiving.RoutingNumber = dealData.DealOffer.RoutingNumber; //GetRoutingNumber((fundingAccount)[0]);

                instructionRequest.ReferenceNumber = drawDownId;

                if ((dealData.DealOffer.AccountType) != null) {
                    if ((Convert.ToString (dealData.DealOffer.AccountType)).ToLower () == "depository") {
                        instructionRequest.Type = MoneyMovement.Ach.TransactionType.CreditToSavings;
                    } else {
                        instructionRequest.Type = MoneyMovement.Ach.TransactionType.CreditToChecking;
                    }
                } {
                    instructionRequest.Type = MoneyMovement.Ach.TransactionType.CreditToSavings;
                }

                instructionRequest.ExecutionDate = drawDownDetail.DrawDownDate.Time.DateTime;
                //     await Task.Run(() => QueueService.Queue(instructionRequest));
                if (bankAccountDetail != null) {
                    if (!string.IsNullOrWhiteSpace (bankAccountDetail.AccountNumber)) {
                        receiving.AccountNumber = bankAccountDetail.AccountNumber;
                        //fundingAccount["AccountType"] = bankAccountDetail.AccountNumber;
                    }
                    if (!string.IsNullOrWhiteSpace (bankAccountDetail.NameOnAccount)) {
                        //TODO : Need to replace Account Name with name on account
                        receiving.Name = bankAccountDetail.NameOnAccount;
                        //fundingAccount["NameOnAccount"] = bankAccountDetail.NameOnAccount;
                    }
                    if (!string.IsNullOrWhiteSpace (bankAccountDetail.RoutingNumber)) {
                        receiving.RoutingNumber = bankAccountDetail.RoutingNumber;
                        //fundingAccount["RoutingNumber"] = bankAccountDetail.RoutingNumber;
                    }
                    if (!string.IsNullOrWhiteSpace (bankAccountDetail.AccountType)) {
                        //fundingAccount["AccountType"] = bankAccountDetail.AccountType;
                        if ((Convert.ToString (dealData.DealOffer.AccountType)).ToLower () == "depository") {
                            instructionRequest.Type = MoneyMovement.Ach.TransactionType.CreditToSavings;
                        } else {
                            instructionRequest.Type = MoneyMovement.Ach.TransactionType.CreditToChecking;
                        }
                    }
                }
                instructionRequest.Receiving = receiving;
                QueueService.Queue (instructionRequest);
                var reasons = new RequestModel ();
                reasons.reasons = new List<string> () { "r10" };
                var statusDetail = await StatusManagementService.GetActiveStatusWorkFlow (EntityType, drawDownId);
                await DrawDownService.UpdatedFundingDetails (drawDownId, new FundingDetailsRequest () { DrawDownFundedRequest = TenantTime.Now, FundedAmount = drawDownDetail.ApprovedAmount - drawDownFeeAmount });

                var drawDownReq = new UpdateDrawDownAmountRequest ();
                drawDownReq.DrawDownAmount = application.TotalDrawDownAmount + drawDownDetail.ApprovedAmount;
                await BusinessApplicationService.UpdateDrawDownAmount (drawDownDetail.LocNumber, drawDownReq);
                await StatusManagementService.ChangeStatus (EntityType, drawDownId, statusDetail.StatusWorkFlowId, DrawDownConfiguration.Statuses["FundingPassed"], new RequestModel ()); //2405018

                // Auto loan schedule creation
                if (isResubmit != true) {
                    await LoanScheduleHandler (EntityType, drawDownId);
                }
            }
        }

        public async Task<IProductRuleResult> ComputeOffer (string entityId) {
            try {
                if (string.IsNullOrWhiteSpace (entityId))
                    throw new ArgumentNullException (nameof (entityId));
                var productId = await GetProductId (EntityType, entityId);

                IProductRuleResult objInitialOffers = null;

                objInitialOffers = await ProductRuleService.RunRule (EntityType, entityId, productId, "DrawDownOffer");
                var statusDetail = await StatusManagementService.GetActiveStatusWorkFlow ("drawdown", entityId);
                if (objInitialOffers.Result == Result.Passed) {
                    var intermidiateResult = JObject.FromObject (objInitialOffers.IntermediateData).ToObject<Dictionary<string, object>> ();

                    if (objInitialOffers != null && objInitialOffers.Result == Result.Passed && intermidiateResult.Count > 0) {
                        await StatusManagementService.ChangeStatus (EntityType, entityId, statusDetail.StatusWorkFlowId, DrawDownConfiguration.Statuses["Approved"], null);
                    }
                } else {
                    List<string> reasons = new List<string> ();
                    reasons.Add ("r9");
                    var requestModel = new RequestModel ();
                    requestModel.reasons = reasons;
                    await StatusManagementService.ChangeStatus (EntityType, entityId, statusDetail.StatusWorkFlowId, DrawDownConfiguration.Statuses["Review"], requestModel);
                }

                return objInitialOffers;
            } catch (Exception ex) {
                Console.WriteLine (ex.Message);
                throw;
            }
        }

        public async Task<string> GetProductId (string entityType, string entityId) {
            string productId = null;
            var applicationAttributes = await DataAttributeEngine.GetAttribute (entityType, entityId, "product");
            if (applicationAttributes == null)
                throw new NotFoundException ($"product DataAttribute not found for {entityId} - {entityType}");

            productId = GetResultValueForProductId (((JArray) applicationAttributes) [0]);
            return productId;
        }

        public async Task FundingStatusHandler (string entityType, string entityId, object data) {

            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (data == null)
                throw new ArgumentNullException (nameof (data));

            var drawdown = await DrawDownService.GetByEntityId (entityId);
            if (drawdown == null)
                throw new ArgumentNullException ($"Application for {nameof(entityId)} {entityId} not exist");

            var applicationData = await BusinessApplicationService.GetByApplicationNumber (drawdown.LocNumber);
            var ApplicationStatusDetail = await StatusManagementService.GetActiveStatusWorkFlow ("application", applicationData.ApplicationNumber);
            if (ApplicationStatusDetail.Code == DrawDownConfiguration.Statuses["Cancelled"]) {
                var statusDetail = await StatusManagementService.GetActiveStatusWorkFlow (EntityType, entityId);
                var objFundingData = ExecuteRequest<ApplicationFunding> (data.ToString ());
                var reasons = new RequestModel ();
                reasons.reasons = new List<string> () { "r10" };
                if (objFundingData != null && objFundingData.RequestStatus.ToLower () == "confirmed") {
                    await StatusManagementService.ChangeStatus (EntityType, entityId, statusDetail.StatusWorkFlowId, DrawDownConfiguration.Statuses["FundingPassed"], reasons);
                    var drawDownReq = new UpdateDrawDownAmountRequest ();
                    drawDownReq.DrawDownAmount = applicationData.TotalDrawDownAmount + drawdown.ApprovedAmount;
                    await BusinessApplicationService.UpdateDrawDownAmount (drawdown.LocNumber, drawDownReq);
                    if (drawDownReq.DrawDownAmount == applicationData.LoanAmount) {
                        var reason = new RequestModel ();
                        reason.reasons = new List<string> () { "r4" };
                        await StatusManagementService.ChangeStatus ("application", applicationData.ApplicationNumber, ApplicationStatusDetail.StatusWorkFlowId, DrawDownConfiguration.Statuses["Declined"], reason);
                    }

                    // Auto loan schedule creation
                    await LoanScheduleHandler (entityType, entityId);
                } else if (objFundingData != null && objFundingData.RequestStatus.ToLower () == "returned") {
                    await StatusManagementService.ChangeStatus (EntityType, entityId, statusDetail.StatusWorkFlowId, DrawDownConfiguration.Statuses["FundingFailed"], reasons);
                }
            }
        }

        public async Task LoanScheduleHandler (string entityType, string entityId) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (!DrawDownConfiguration.AutoLoanSchedule)
                return;

            var drawdown = await DrawDownService.GetByEntityId (entityId);
            var loanInformation = await LoanManagementService.GetLoanInformationByApplicationNumber (drawdown.LocNumber);
            if (loanInformation == null)
                throw new ArgumentException ($"Loan details not available for loan number: {drawdown.LocNumber}");

            List<ILoanSchedule> loanSchedules = null;
            try {
                loanSchedules = await LoanManagementService.GetLoanScheduleByLoanNumber (loanInformation.LoanNumber);
            } catch (Exception) { }

            var scheduleVersionNo = "v1";
            if (loanSchedules != null)
                scheduleVersionNo = "v" + (loanSchedules.Count + 1);

            var product = await GetProductParameters ("loan", loanInformation.LoanNumber);
            var dealData = await OfferEngineService.GetApplicationOffers ("application", drawdown.LocNumber);
            if (dealData == null)
                throw new ArgumentNullException (nameof (entityId));

            var paymentFrequency = dealData.DealOffer.DurationType.ToLower () == "daily" ? PaymentFrequency.Daily : PaymentFrequency.Weekly;

            var loanScheduleRequest = new LoanScheduleRequest ();
            loanScheduleRequest.LoanNumber = loanInformation.LoanNumber;
            loanScheduleRequest.CreditLimit = dealData.DealOffer.LoanAmount;
            loanScheduleRequest.DrawDownNumber = drawdown.EntityId;
            loanScheduleRequest.FactorRate = dealData.DealOffer.SellRate;
            loanScheduleRequest.PaymentFrequency = paymentFrequency;
            loanScheduleRequest.FundedAmount = drawdown.FundedAmount;
            loanScheduleRequest.FundedDate = drawdown.DrawDownFundedDate.Time.DateTime;
            loanScheduleRequest.LoanAmount = drawdown.ApprovedAmount;
            loanScheduleRequest.ScheduledCreatedReason = "drawdowncreate";
            loanScheduleRequest.ScheduleNotes = "New drawdown added";
            loanScheduleRequest.ScheduleVersionNo = scheduleVersionNo;
            loanScheduleRequest.Tenure = CalculateTenure (dealData.DealOffer.Term, loanScheduleRequest.PaymentFrequency);
              loanScheduleRequest.NumberOfDaysInMonth = DrawDownConfiguration.NumberOfBusinessDaysInMonth;
         loanScheduleRequest.NumberOfDaysInWeek = DrawDownConfiguration.NumberOfBusinessDaysInWeek;
             loanScheduleRequest.SlabDetails =  dealData.DealOffer.Term == 8 ? DrawDownConfiguration.SlabDetails8Month :   DrawDownConfiguration.SlabDetails;
            loanScheduleRequest.TermInMonth = dealData.DealOffer.Term;
                 
            if (drawdown.DrawDownFeeAmount > 0) {
                loanScheduleRequest.Fees = new List<LMS.LoanManagement.Abstractions.IFee> ();
                var preappliedFee = product != null && product.ProductFeeParameters != null ? product.ProductFeeParameters.Where (p => p.FeeType == "preapplied").FirstOrDefault () : null;
                if (preappliedFee != null) {
                    var fee = new LMS.LoanManagement.Abstractions.Fee ();
                    fee.Applied = true;
                    fee.FeeAmount = drawdown.DrawDownFeeAmount;
                    fee.FeeName = preappliedFee.Name;
                    fee.FeeType = preappliedFee.FeeType;
                    loanScheduleRequest.Fees.Add (fee);
                }
            }
            //loanScheduleRequest.SlabDetails = dealData.ter 
           /* if (DrawDownConfiguration.PaybackTiers != null) {
                //var noOfPayments = loanScheduleRequest.PaymentFrequency == PaymentFrequency.Weekly ? WeekConstant : WeekConstant * 7;
                var noOfPayments = loanScheduleRequest.PaymentFrequency == PaymentFrequency.Weekly ? DrawDownConfiguration.NumberOfBusinessDaysInWeek : DrawDownConfiguration.NumberOfBusinessDaysInMonth;
                DrawDownConfiguration.PaybackTiers[0].NoOfPayments = Convert.ToInt32 (2 * noOfPayments);
                DrawDownConfiguration.PaybackTiers[1].NoOfPayments = Convert.ToInt32 (4 * noOfPayments);
                loanScheduleRequest.PaybackTiers = new List<IPaybackTier> ();
                loanScheduleRequest.PaybackTiers.AddRange (DrawDownConfiguration.PaybackTiers);
            }*/

            var loanStatus = await StatusManagementService.GetActiveStatusWorkFlow ("loan", loanInformation.LoanNumber);
            if (loanStatus == null)
                throw new ArgumentException ($"Loan status not available for loan number: {loanInformation.LoanNumber}");

            if (loanStatus.Code == "200.30" || loanStatus.Code == "200.40") {
                await StatusManagementService.ChangeStatus ("loan", loanInformation.LoanNumber, loanStatus.StatusWorkFlowId, "200.20", new RequestModel ());
            }

            // Create new loan schedule.
            await LoanManagementService.LoanSchedule (loanScheduleRequest);

            // Initialize created schedule.
            await LoanManagementService.InitializeLoan (loanInformation.LoanNumber);
        }

        public async Task GenerateLocAgreement (string drawDownId) {
            if (string.IsNullOrWhiteSpace (drawDownId))
                throw new ArgumentNullException (nameof (drawDownId));

            var drawDown = await DrawDownService.GetByEntityId (drawDownId);

            if (drawDown == null)
                throw new InvalidOperationException ("drawdown not found");

            var application = await BusinessApplicationService.GetByApplicationNumber (drawDown.LocNumber);

            if (application == null)
                throw new InvalidOperationException ("Application not found");

            var factDetails = await VerificationEngineService.GetFactVerification (EntityType, drawDownId, application.ProductId, false);

            if (factDetails != null && factDetails.Any ()) {
                var incompletFacts = factDetails.Where (fact => fact.VerificationStatus != VerificationEngine.VerificationStatus.Passed);
                if (incompletFacts != null && incompletFacts.Any ()) {
                    throw new IncompleteChecklistException ("Pending Verification");
                }
            }

            var applicant = await BusinessApplicantService.Get (application.ApplicantId);

            if (applicant == null)
                throw new InvalidOperationException ("Applicant not found");

            var loanInfo = await LoanManagementService.GetLoanInformationByApplicationNumber (drawDown.LocNumber);

            if (loanInfo == null)
                throw new System.Exception ("Loan Information Does Not Exist");

            var applicationOffers = await OfferEngineService.GetApplicationOffers ("application", drawDown.LocNumber);

            if (applicationOffers == null && applicationOffers.DealOffer == null)
                throw new System.Exception ("DealOffer Does Not Exist");
            
            //Get Previous Drawdown Amount 
            double mostRecentPurchasePrice = 0;
            var drawdownListForApp = await DrawDownService.GetByLocNumber(drawDown.LocNumber);
            if(drawdownListForApp.Count > 1)
            {
            var drawdownListForAppsorted = drawdownListForApp.ToList<IDrawDown>().OrderBy(x=>x.EntityId).ToList<IDrawDown>();
            foreach(var currentdrawdown in drawdownListForAppsorted)
            {                 
                 if(currentdrawdown.EntityId ==  drawDown.EntityId)
                   break;
                mostRecentPurchasePrice = currentdrawdown.ApprovedAmount;
            }
            }
            //

            var deal = applicationOffers.DealOffer;
            IDrawDown currDrawdown = drawDown;
            IAccrual accrualData = null;
            double prevOutstanding = 0;

            try {
                accrualData = await AccrualBalanceService.GetAccrualByLoanApplicationNumber (drawDown.LocNumber);
            } catch (Exception e) { }

            IPBOT loanData;
            double loanAmount, oldRepaymentAmount, newRepayAmount, totalPayback, newFactorRate, FinanceFee;
            double totaloutstanding ;
            if (accrualData != null) {
                loanData = accrualData.PBOT;
                loanAmount =  currDrawdown.ApprovedAmount; //loanData.TotalPrincipalOutStanding + currDrawdown.ApprovedAmount;
                oldRepaymentAmount = loanData.TotalPrincipalOutStanding ; //* accrualData.FactorRate;
                newRepayAmount = currDrawdown.ApprovedAmount * deal.SellRate;
                //totalPayback = oldRepaymentAmount + newRepayAmount;
                totalPayback = (currDrawdown.ApprovedAmount + loanData.TotalPrincipalOutStanding) * deal.SellRate;
                newFactorRate = (totalPayback / loanAmount);
                FinanceFee = totalPayback - loanAmount;
                 prevOutstanding = loanData.TotalPrincipalOutStanding;
               // prevOutstanding = loanData.TotalPrincipalOutStanding * accrualData.FactorRate;
                totaloutstanding = currDrawdown.ApprovedAmount + loanData.TotalPrincipalOutStanding;
            } else {
                loanAmount = currDrawdown.ApprovedAmount;
                totalPayback = currDrawdown.ApprovedAmount * deal.SellRate;
                newFactorRate = deal.SellRate;
                FinanceFee = totalPayback - loanAmount;
              //  prevOutstanding = deal.LoanAmount - currDrawdown.ApprovedAmount;
               totaloutstanding = currDrawdown.ApprovedAmount;
                 prevOutstanding = totaloutstanding;
            }
           //prevOutstanding = totaloutstanding * deal.SellRate;;
          
            if (applicationOffers?.DealOffer != null) {
                //var deal = applicationOffers.DealOffer;
                var paymentFrequency = (deal.DurationType == "Weekly" ? PaymentFrequency.Weekly : PaymentFrequency.Daily);
                  
                  
                ILoanScheduleRequest currLoanSchedObj = new LoanScheduleRequest () {
                    FactorRate = deal.SellRate,
                    LoanAmount = loanAmount, // deal.LoanAmount, //deal.LoanAmount,kt
                    PaymentFrequency = paymentFrequency,
                    Tenure = CalculateTenure (deal.Term, paymentFrequency),
                    FundedDate = DateTime.Now,
                    AnnualRate = 5,
                    NumberOfDaysInMonth = DrawDownConfiguration.NumberOfBusinessDaysInMonth,
                    NumberOfDaysInWeek = DrawDownConfiguration.NumberOfBusinessDaysInWeek,
                   // SlabDetails = DrawDownConfiguration.SlabDetails
                    SlabDetails =  deal.Term == 8 ? DrawDownConfiguration.SlabDetails8Month :   DrawDownConfiguration.SlabDetails,
                    
                }; 
                 
                 ILoanAmortizationRequest customRequest = new LoanAmortizationRequest()
                 {
                    FactorRate = deal.SellRate,
                    LoanAmount = totaloutstanding, // deal.LoanAmount, //deal.LoanAmount,loanAmount
                    PaymentFrequency = paymentFrequency,                   
                    NumberOfDaysInMonth = DrawDownConfiguration.NumberOfBusinessDaysInMonth,
                    NumberOfDaysInWeek = DrawDownConfiguration.NumberOfBusinessDaysInWeek,
                    SlabDetails =  deal.Term == 8 ? DrawDownConfiguration.SlabDetails8Month :   DrawDownConfiguration.SlabDetails,
                    TermInMonth = deal.Term,
                    RoundingDigit = DrawDownConfiguration.RoundingDigit,
                    RoundingMethod = DrawDownConfiguration.RoundingMethod
                    
                 };
                 List<AmortizationResponse> amortizationResponse = LoanAmortization.GetFcAmortization(customRequest);

                 var currScheduleDetails = new List<ILoanScheduleDetails> ();
                 currScheduleDetails = LoanManagementService.Amortize (currLoanSchedObj, deal.LoanAmount, application.PortfolioType.ToString (), "mathematical", 2, deal.SellRate);
                  // var schedules = LoanAmortization.GetAmortizationScedule(requestParam);
                //var loanInfo = await LoanManagementService.GetLoanInformationByApplicationNumber(entityId);
               //  urrScheduleDetails = LoanManagementService.Amortize(currLoanSchedObj, deal.LoanAmount, application.PortfolioType.ToString(), "mathematical", 2);
                // var schedules = LoanAmortization.GetAmortizationScedule(requestParam);
                LoanModificationEventRequest payload = new LoanModificationEventRequest () {
                    loanAmount = loanAmount, //deal.LoanAmount,
                    loanSchedule = currScheduleDetails,
                    frequency = deal.DurationType,
                    term = deal.Term
                };
                //monthlySchedule
               // IProductRuleResult response = new ProductRuleResult ();

               // response = await ProductRuleService.RunRule (EntityType, drawDown.LocNumber, application.ProductId, "ModifyLoanSchedule", payload);
                //if (response.Result == Result.Passed) {
                    double percentage = Convert.ToDouble (DrawDownConfiguration.ProfessionalServiceFeePercentage);
                    //var body = new LocLoanAgreementDocument (applicant, applicationOffers.DealOffer, application, response.IntermediateData, loanAmount, percentage,amortizationResponse);
                    var body = new LocLoanAgreementDocument (applicant, applicationOffers.DealOffer, application, null, loanAmount, percentage,amortizationResponse,prevOutstanding,totaloutstanding,totalPayback,deal.Term,DrawDownConfiguration.NumberOfBusinessDaysInMonth,DrawDownConfiguration.NumberOfBusinessDaysInWeek,mostRecentPurchasePrice);
                    body.LenderName = DrawDownConfiguration.LenderSignature.Name;
                    body.Logo = DrawDownConfiguration.Logo;
                    var agreementDocument = new LendFoundry.DocumentGenerator.Document {
                        Data = body,
                        Format = DocumentFormat.Pdf,
                       // Name = "AddendumAgreement.pdf",
                        Name = DrawDownConfiguration.DocumentName,
                        Version = "1.0"
                    };
                    var agreementResult = await DocumentGeneratorService.Generate ("AddendumAgreement", "1.0", Format.Html, agreementDocument);
                    var MetaObj = new {
                        TemplateName = "AddendumAgreement",
                        TemplateVersion = "1.0"
                    };
                    if (agreementResult != null) {
                        var loandApplicationDocument = await ApplicationDocumentService.Add (EntityType, drawDownId, "LoanAgreement", agreementResult.Content, agreementResult.DocumentName, new List<string> (), MetaObj);
                    }
                //}
            }
        }

        public async Task SendToBorrower (string drawDownId, string docusignReturnUrl) {
            if (string.IsNullOrWhiteSpace (drawDownId))
                throw new ArgumentNullException (nameof (drawDownId));

            var drawDown = await DrawDownService.GetByEntityId (drawDownId);

            if (drawDown == null)
                throw new NotFoundException ($"No drawdown found for {drawDownId}");

            var statusDetail = await StatusManagementService.GetActiveStatusWorkFlow (EntityType, drawDownId);

            if (statusDetail == null)
                throw new InvalidOperationException ($"Drawdown status detail not found for drawdown number : {drawDownId}");

            var currentStatus = await StatusManagementService.GetStatusByEntity (EntityType, drawDownId, statusDetail.StatusWorkFlowId);

            if (currentStatus == null || currentStatus.Code != DrawDownConfiguration.Statuses["Approved"])
                throw new InvalidOperationException ($"Can not send agreement to borrower when status is not : {DrawDownConfiguration.Statuses["Approved"]}");

            IEnumerable<IDocumentWithApplication> AgreementDocument = await ApplicationDocumentService.GetByApplicationNumber (EntityType, drawDownId);
            //var LoanAgreement = AgreementDocument.LastOrDefault (f => f.FileName.Contains ("AddendumAgreement"));
            var LoanAgreement = AgreementDocument.LastOrDefault (f => f.FileName.Contains (DrawDownConfiguration.DocumentName));
            if (LoanAgreement != null) {
                var issuedToken = await GenerateToken (EntityType, drawDownId, 0);
                var eventName = EntityType + "LoanAgreementOut";
                await EventHub.Publish (eventName, new {
                    EntityId = drawDownId,
                        EntityType = EntityType,
                        Response = "",
                        Request = issuedToken,
                        ReferenceNumber = Guid.NewGuid ().ToString ("N")
                });
                await StatusManagementService.ChangeStatus (EntityType, drawDownId, statusDetail.StatusWorkFlowId, DrawDownConfiguration.Statuses["AgreementVerification"], new RequestModel ());
                var docusingResult = await GetAgreementDocument (drawDownId, docusignReturnUrl);
            } else {
                throw new InvalidOperationException ("Addendum Agreement Not generated");
            }
        }

        public async Task<DocusignReturnObject> GetAgreementDocument (string drawDownId, string docusignReturnUrl) {
            if (string.IsNullOrWhiteSpace (drawDownId))
                throw new ArgumentNullException (nameof (drawDownId));

            var drawdown = await DrawDownService.GetByEntityId (drawDownId);
            if (drawdown == null)
                throw new NotFoundException ($"No drawdown found for {drawDownId}");

            var application = await BusinessApplicationService.GetByApplicationNumber (drawdown.LocNumber);

            if (application == null)
                throw new InvalidOperationException ("Application not found");

            var applicant = await BusinessApplicantService.Get (application.ApplicantId);
            if (applicant == null)
                throw new InvalidOperationException ("Applicant not found");
            try {
                IEnumerable<IDocumentWithApplication> AgreementDocument = await ApplicationDocumentService.GetByApplicationNumber (EntityType, drawDownId);
                IDocumentWithApplication LoanAgreement = null;
                var list = new List<string> ();

                //LoanAgreement = AgreementDocument.LastOrDefault (f => f.FileName.Contains ("AddendumAgreement"));
                LoanAgreement = AgreementDocument.LastOrDefault (f => f.FileName.Contains (DrawDownConfiguration.DocumentName));
                list.Add (LoanAgreement.Id);

                var DocusignObject = await AddendumDocusignUrl (drawdown, applicant, application, list, docusignReturnUrl);
                return new DocusignReturnObject () { url = DocusignObject.Url, isDocusignCreated = true };
            } catch (System.Exception e) {
                return new DocusignReturnObject () { url = null, isDocusignCreated = false };
            }
        }
        public async Task<IEmbeddedUrlResponse> AddendumDocusignUrl (IDrawDown drawdown, IApplicant applicant, IApplication application, List<string> documentIds, string returnUrl) {
            if (documentIds != null) {
                Random random = new Random ();
                var entityId = drawdown.EntityId;
                var clientId = random.Next ().ToString ();
                LendFoundry.Syndication.LFDocuSign.Request.IEmbedSigningRequest signingRequestObj = new LendFoundry.Syndication.LFDocuSign.Request.EmbedSigningRequest ();
                signingRequestObj.DocumentId = new List<string> ();
                signingRequestObj.DocumentId = documentIds;
                signingRequestObj.ClientId = clientId;
                signingRequestObj.EmailBody = "";
                signingRequestObj.Receipients = new List<IReceipient> ();

                applicant.Owners = applicant.Owners.OrderByDescending (i => i.IsPrimary).ToList ();

                var primaryOwner = applicant.Owners?.FirstOrDefault (x => x.IsPrimary == true);
                var secondaryOwners = applicant.Owners?.Where (x => x.IsPrimary != true);
                if (primaryOwner != null) {
                    var receipient = new Receipient () {
                    Email = primaryOwner.EmailAddresses.FirstOrDefault ().Email,
                    Name = $"{primaryOwner.FirstName} {primaryOwner.LastName}",
                    ClientId = null,
                    RoutingNumber = 1
                    };
                    signingRequestObj.Receipients.Add (receipient);
                    if (secondaryOwners != null) {
                        int routingNumber = 1;
                        foreach (var secondaryOwner in secondaryOwners) {
                            routingNumber++;
                            var rec2 = new Receipient () {
                                Email = secondaryOwner.EmailAddresses.FirstOrDefault ().Email,
                                Name = $"{secondaryOwner.FirstName} {secondaryOwner.LastName}",
                                ClientId = null,
                                RoutingNumber = routingNumber
                            };
                            signingRequestObj.Receipients.Add (rec2);
                        }
                    }
                    var rec3 = new Receipient () {
                        Email = DrawDownConfiguration.LenderSignature.Email,
                        Name = DrawDownConfiguration.LenderSignature.Name,
                        ClientId = null,
                        RoutingNumber = 5
                    };
                    signingRequestObj.Receipients.Add (rec3);
                }

                var response = await DocuSignService.GenerateEmbeddedView (EntityType, entityId, "LoanAgreement", signingRequestObj);

                LendFoundry.Syndication.LFDocuSign.Request.IGenerateEmbeddedUrlRequest urlRequestObj = new LendFoundry.Syndication.LFDocuSign.Request.GenerateEmbeddedUrlRequest () {
                    EnvelopeId = response.EnvelopeSummary.EnvelopeId,
                    ClientUserId = clientId.ToString (),
                    ReturnUrl = returnUrl,
                    SignerEmail = primaryOwner.EmailAddresses.FirstOrDefault ().Email,
                    SignerName = $"{primaryOwner.FirstName} {primaryOwner.LastName}",
                    RecipientId = response.RecepientIds?[0]
                };

                return await DocuSignService.GenerateEmbeddedUrl (EntityType, entityId, urlRequestObj);
            } else {
                throw new System.Exception ("Error occured while initializing docusign");
            }
        }

        public async Task<bool> SignDocumentReceivedHandler (string entityType, string entityId, object data) {
            if (String.IsNullOrWhiteSpace (entityType))
                throw new ArgumentNullException (nameof (entityType));

            if (String.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            var result = string.Empty;
            entityType = EnsureEntityType (entityType);

            if (data == null)
                throw new ArgumentNullException (nameof (data));

            var drawDown = await DrawDownService.GetByEntityId (entityId);
            if (drawDown == null)
                throw new NotFoundException ($"No drawdown found for {entityId}");

            var locStatusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow ("application", drawDown.LocNumber);
            if (locStatusWorkFlow == null)
                throw new NullReferenceException (nameof (locStatusWorkFlow));

            if (locStatusWorkFlow.Code == DrawDownConfiguration.Statuses["LoCOpen"]) {
                var application = await BusinessApplicationService.GetByApplicationId (drawDown.LocNumber);

                if (application == null)
                    throw new ArgumentNullException ($"Application for {nameof(entityId)} {entityId} not exist");

                var objDocuSignData = ExecuteRequest<DocuSignData> (data.ToString ());

                var refNumber = Guid.NewGuid ().ToString ("N");
                var issuedToken = await GenerateToken (EntityType, entityId, 0);
                var totalDocs = objDocuSignData.DocumentContent != null ? objDocuSignData.DocumentContent.Count () : 1;
                if (objDocuSignData.DocumentContent == null) {
                    throw new NotFoundException ("DocumentContent not found");
                }
                //for (int i = 0; i < totalDocs; i++)
                //{
                //    await ApplicationDocumentService.Add(entityId, ApplicationProcessorConfiguration.DocumentCategory["SignedDocument"], objDocuSignData.DocumentContent[i], objDocuSignData.DocumentName[i], null, null);
                //}
                if (objDocuSignData.DocumentContent != null) {
                    var StatusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow (EntityType, entityId);
                    if (StatusWorkFlow == null)
                        throw new NullReferenceException (nameof (StatusWorkFlow));
                    string workflowName = StatusWorkFlow.StatusWorkFlowId;
                    for (int i = 0; i < totalDocs; i++) {
                        await VerificationEngineService.AddDocument (EntityType, entityId, workflowName, "SignedContractVerification", "SignedContractVerification", objDocuSignData.DocumentContent[i], objDocuSignData.DocumentName[i]);
                    }
                }
                await EventHub.Publish (new SignedLoanAgreement {
                    EntityId = entityId,
                        EntityType = EntityType,
                        Response = "",
                        Request = issuedToken,
                        ReferenceNumber = refNumber
                });
            }
            return true;
        }
        private string EnsureEntityType (string entityType) {
            if (string.IsNullOrWhiteSpace (entityType))
                throw new InvalidArgumentException ("Invalid Entity Type");

            entityType = entityType.ToLower ();
            var validEntityTypes = LookupService.GetLookupEntry ("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any ())
                throw new InvalidArgumentException ("Invalid Entity Type");

            return entityType;
        }

        public async Task AddFundingRequest (string drawDownId) {
            if (string.IsNullOrWhiteSpace (drawDownId))
                throw new ArgumentNullException (nameof (drawDownId));

            var drawDownDetail = await DrawDownService.GetByEntityId (drawDownId);

            if (drawDownDetail == null)
                throw new InvalidOperationException ("Application not found");

            var application = await BusinessApplicationService.GetByApplicationNumber (drawDownDetail.LocNumber);

            var factDetails = await VerificationEngineService.GetFactVerification (EntityType, drawDownId, application.ProductId, false);

            if (factDetails != null && factDetails.Any ()) {
                var incompletFacts = factDetails.Where (fact => fact.VerificationStatus != VerificationEngine.VerificationStatus.Passed);
                if (incompletFacts != null && incompletFacts.Any ()) {
                    throw new IncompleteChecklistException ("Pending Verification");
                }
            }

            var dealData = await DataAttributeEngine.GetAttribute ("application", application.ApplicationNumber, "selatedDealOffer");
            await DrawDownService.UpdatedFeeDetails (drawDownId, new FeeDetailsRequest () { DrawDownFeeAmount = 0 });

            IFundingRequest request = new FundingRequest ();
            request.AmountFunded = Convert.ToDouble (GetAmountFunded (((JArray) dealData) [0]));
            request.BankAccountNumber = GetAccountNumber (((JArray) dealData) [0]);
            request.BankAccountType = GetAccountType (((JArray) dealData) [0]);
            request.BankRTN = GetRoutingNumber (((JArray) dealData) [0]);
            request.BorrowersName = $"test";
            request.RequestedDate = DateTime.UtcNow;

            await FundingService.AddFundingRequest (EntityType, drawDownId, request);
            var statusDetail = await StatusManagementService.GetActiveStatusWorkFlow ("drawdown", drawDownId);
            await StatusManagementService.ChangeStatus (EntityType, drawDownId, statusDetail.StatusWorkFlowId, DrawDownConfiguration.Statuses["FundingPassed"], new RequestModel ());
        }

        #region Private Methods

        private async Task<IProductRuleResult> ComputedOfferTemp (string entityId) {
            var productId = await GetProductId (EntityType, entityId);
            IProductRuleResult objInitialOffers = null;
            objInitialOffers = await ProductRuleService.RunRule (EntityType, entityId, productId, "DrawDownOffer");
            return objInitialOffers;
        }

        private T ExecuteRequest<T> (string response) where T : class {
            try {
                return JsonConvert.DeserializeObject<T> (response);
            } catch (System.Exception exception) {
                throw new System.Exception ("Unable to deserialize:" + response, exception);
            }
        }

        private static string GetAccountNumber (dynamic data) {
            Func<dynamic> resultProperty = () => data.accountNumber;

            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }

        private static string GetAccountType (dynamic data) {
            Func<dynamic> resultProperty = () => data.accountType;

            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }

        private static string GetRoutingNumber (dynamic data) {
            Func<dynamic> resultProperty = () => data.routingNumber;

            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }

        private static double GetAmountFunded (dynamic data) {
            Func<dynamic> resultProperty = () => data.amountFunded;

            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }

        private static double GetLoanAmount (dynamic data) {
            Func<dynamic> resultProperty = () => data.approveAmount;

            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }

        private static string GetResultValueForProductId (dynamic data) {
            Func<dynamic> resultProperty = () => data.ProductId;
            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }

        private static bool HasProperty<T> (Func<T> property) {
            try {
                property ();
                return true;
            } catch (RuntimeBinderException) {
                return false;
            }
        }

        private static T GetValue<T> (Func<T> property) {
            return property ();
        }

        private IDrawDownRequest GetDrawDownRequest (IDrawDownAddRequest request) {
            var drawDownRequest = new DrawDownRequest {
                RequestedAmount = request.RequestedAmount,
                LocNumber = request.LocNumber,
                DrawDownDate = request.DrawDownDate,
                NeedByDate = request.NeedByDate
            };

            return drawDownRequest;
        }

        /*private int CalculateTenure (int term, PaymentFrequency paymentFrequency) {
            switch (paymentFrequency) {
                case PaymentFrequency.Weekly:
                    return Convert.ToInt32 (WeekConstant * term);
                case PaymentFrequency.Daily:
                    return Convert.ToInt32 ((WeekConstant * term * 7));
                default:
                    throw new ArgumentException ("Invalid payment frequency");
            }
        }*/
        private int CalculateTenure (int term, PaymentFrequency paymentFrequency) {

            double monthParam = Convert.ToDouble (DrawDownConfiguration.NumberOfBusinessDaysInMonth); // loanProductPrameters.FirstOrDefault(x => x.ParameterId == "NUM_BIZ_DAYS_IN_MNTH");
            double weekParam = Convert.ToDouble (DrawDownConfiguration.NumberOfBusinessDaysInWeek); //loanProductPrameters.FirstOrDefault(x => x.ParameterId == "NUM_BIZ_DAYS_IN_WEEK");
            var paramValue = monthParam;
            var weekParamValue = weekParam;
            if (paymentFrequency == PaymentFrequency.Daily) {
                return Convert.ToInt16 (term * paramValue);
            } else if (paymentFrequency == PaymentFrequency.Weekly) {
              return Convert.ToInt16(Math.Ceiling((term * paramValue) / weekParamValue));
            } else if (paymentFrequency == PaymentFrequency.BiWeekly) {
                 return Convert.ToInt32(Math.Ceiling((term * paramValue) / (weekParamValue * 2)));
            } else if (paymentFrequency == PaymentFrequency.Monthly) {
                return term;
            } else if (paymentFrequency == PaymentFrequency.Quarterly) {
                return term * 3;
            } else {
                return 0;
            }

        }

        private async Task<ProductDetails> GetProductParameters (string entityType, string entityId) {
            var productDetails = await DataAttributeEngine.GetAttribute (entityType, entityId, "product");
            var productObject = JsonConvert.DeserializeObject<List<ProductDetails>> (productDetails.ToString ());
            return productObject.FirstOrDefault ();
        }

        public Task ACHFailStatusHandler (string entityType, string entityId, object data) {
            throw new NotImplementedException ();
        }
        #endregion

        #region Syndication call
        private async Task SyndicationCall (string drawdownId, IApplication applicationDetail, IApplicant applicant, IList<IOwner> ownerList = null) {
            if (applicationDetail != null) {
            var request = new {
            City = applicationDetail.PrimaryAddress.City,
            Email = applicationDetail.PrimaryEmail.Email,
            FirstName = applicationDetail.ContactFirstName,
            LastName = applicationDetail.ContactLastName,
            Phone = applicant.PrimaryPhone.Phone,
            ZipCode = applicationDetail.PrimaryAddress.ZipCode,
            State = applicationDetail.PrimaryAddress.State,
            BusinessAddress = applicationDetail.PrimaryAddress.AddressLine1,
            ClientIpAddress = applicationDetail.ClientIpAddress,
            BusinessName = applicant.LegalBusinessName,
            FederalSalesTaxID = applicant.BusinessTaxID,
            DBA = applicant.DBA,
            BusinessPhone = applicationDetail.BusinessPhone.Phone
                };
                /*Parallel.Invoke(() => BizApi(applicationNumber, request),
                              () => dataMerch(applicationNumber, applicant.BusinessTaxID, DataMerchService),
                              () => whitePage(applicationNumber, request, WhitepagesService),
                              () => Yelp(applicationNumber, request.BusinessPhone, YelpService));*/

                //Equifax integration 
                foreach (IOwner owner in applicant.Owners) {
                    var creditReportRequestModel = this.CreditReportRequestModel (owner, drawdownId);
                    var equifaxCreditReport = await GetEquifaxCreditReport (EntityType, drawdownId, creditReportRequestModel);
                    await DataAttributeEngine.SetAttribute (EntityType, drawdownId, "EquifaxDetail", new { EquifaxDetail = equifaxCreditReport });

                    var issuedToken = await GenerateToken (EntityType, drawdownId, 0);
                    await EventHub.Publish (new PersonalCreditReportAdded {
                        EntityId = drawdownId,
                            EntityType = EntityType,
                            Response = equifaxCreditReport,
                            Request = new { token = issuedToken, OwnerId = owner.OwnerId },
                            ReferenceNumber = Guid.NewGuid ().ToString ("N")
                    });
                }
                CallDataMerch (drawdownId, applicant.BusinessTaxID);
            }
        }
        private ICreditReportRequest CreditReportRequestModel (IOwner ownerData, string entityId) {
            return new CreditReportRequest {
                City = (ownerData.Addresses != null && ownerData.Addresses.Any ()) ? ownerData.Addresses.First ().City : string.Empty,
                    FirstName = ownerData.FirstName,
                    LastName = ownerData.LastName,
                    Ssn = ownerData.SSN,
                    DateOfBirth = Convert.ToDateTime (ownerData.DOB),
                    State = (ownerData.Addresses != null && ownerData.Addresses.Any ()) ? ownerData.Addresses.First ().State : string.Empty,
                    StreetName = (ownerData.Addresses != null && ownerData.Addresses.Any ()) ? ownerData.Addresses.First ().AddressLine1 : string.Empty,
                    //StreetNumber = ownerData.Addresses.FirstOrDefault().
                    ZipCode = (ownerData.Addresses != null && ownerData.Addresses.Any ()) ? ownerData.Addresses.First ().ZipCode : string.Empty,
                    EntityId = entityId,
                    EntityType = EntityType,
            };
        }
        private async void CallDataMerch (string entityId, string federalSalesTaxID) {
            try {
                federalSalesTaxID = federalSalesTaxID.Substring (2);
                await DataMerchService.SearchMerchant (EntityType, entityId, federalSalesTaxID);
            } catch (System.Exception ex) { }
        }
        private async Task<ITokenGenerate> GenerateToken (string entityType, string entityId, int expirationDays = 0) {
            TokenGenerate token = new TokenGenerate ();
            try {
                token.Tenant = Environment.GetEnvironmentVariable ("TenantId");
                token.EntityId = entityId;
                token.EntityType = entityType;
                token.ExpirationDays = expirationDays;
                return await TokenGeneratorService.Issue (token);
            } catch (System.Exception ex) {
                return token;
            }
        }
        private async Task<ICreditReport> GetEquifaxCreditReport (string entityType, string entityId, ICreditReportRequestKey creditReportRequest) {
            ICreditReport result = null;
            try {
                result = await Task.Run (() => EquifaxCreditReportService.CreateReport (entityType, entityId, creditReportRequest));
                return result;
            } catch (System.Exception e) { }
            return result;
        }

        public Task UpdateFundingStatus (string entityId, string Status) {
            throw new NotImplementedException ();
        }

        #endregion
    }
}