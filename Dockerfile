FROM registry.lendfoundry.com/base:beta8

ENV NUGET_FEED_LENDFOUNDRY http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/

ADD ./src/LendFoundry.DrawDownProcessor.Abstractions /app/LendFoundry.DrawDownProcessor.Abstractions
WORKDIR /app/LendFoundry.DrawDownProcessor.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.DrawDownProcessor /app/LendFoundry.DrawDownProcessor
WORKDIR /app/LendFoundry.DrawDownProcessor
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.DrawDownProcessor.Api /app/LendFoundry.DrawDownProcessor.Api
WORKDIR /app/LendFoundry.DrawDownProcessor.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel